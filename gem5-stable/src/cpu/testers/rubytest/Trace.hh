#define TRACE 1
#define WRITE_HITS 0
// BASELINE 1 does not use proposed solution
// BASELINE 0 uses proposed solution
#define NPROC 1
#define NPROC_C 1
#define NPROC_NC 0
#define ALL_CRITICAL 1
#define BASELINE 0
#define SLOT_WIDTH 50 
#define MAX_NPROC 8
#define EEMBC_SPEC 0
