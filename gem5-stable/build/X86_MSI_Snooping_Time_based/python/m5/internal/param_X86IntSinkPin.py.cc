#include "sim/init.hh"

namespace {

const uint8_t data_m5_internal_param_X86IntSinkPin[] = {
    120,156,197,88,255,111,219,198,21,127,71,82,180,37,75,177,
    28,219,113,226,100,53,135,193,152,86,44,246,150,45,109,177,
    6,193,186,118,64,51,108,78,70,117,115,170,181,227,40,242,
    36,83,166,72,129,60,199,85,97,255,50,7,219,126,219,31,
    49,236,135,253,31,251,191,182,247,222,145,148,236,218,109,129,
    13,138,45,30,142,199,187,119,239,203,231,125,185,11,160,248,
    171,225,243,115,7,32,239,9,128,16,127,2,98,128,177,0,
    124,23,82,64,184,14,199,53,200,126,10,97,13,94,3,244,
    12,144,6,92,96,199,132,63,24,144,52,121,141,13,177,201,
    35,2,166,13,144,22,244,106,112,152,172,129,37,109,56,110,
    64,246,39,16,66,36,2,94,134,75,16,46,195,107,164,142,
    157,58,19,92,134,176,193,157,58,132,43,220,105,192,180,13,
    114,5,122,72,124,9,122,45,36,245,54,146,186,197,164,254,
    77,164,66,252,178,1,97,139,166,35,47,159,210,76,139,102,
    242,30,183,152,202,106,201,89,27,122,107,101,255,246,92,127,
    157,251,171,32,215,96,180,1,163,77,24,221,129,1,10,220,
    174,168,110,129,52,97,116,23,122,119,65,226,111,11,46,80,
    39,225,218,220,138,123,188,226,118,181,98,155,87,220,135,222,
    125,144,248,219,214,43,108,232,118,54,81,207,209,127,240,175,
    131,122,6,213,196,230,149,204,242,40,77,188,40,25,164,145,
    65,223,109,106,200,42,1,53,75,133,121,62,36,243,252,11,
    216,54,161,81,152,231,28,144,176,32,89,98,3,206,185,115,
    110,192,180,3,103,2,70,22,132,38,156,225,54,53,98,96,
    40,224,194,128,207,76,154,112,142,173,133,10,125,11,44,165,
    109,51,98,133,106,74,75,112,94,131,179,26,116,95,158,25,
    52,112,92,135,236,159,240,229,3,38,186,204,68,13,56,195,
    214,130,11,11,206,109,56,196,73,56,52,170,147,248,226,229,
    25,74,138,35,221,142,133,220,30,204,137,75,162,132,81,150,
    248,99,169,54,176,239,77,252,204,31,123,47,223,123,231,89,
    162,186,81,114,252,34,74,58,141,114,102,154,239,77,124,117,
    228,242,82,147,116,50,158,40,38,153,38,82,173,96,103,16,
    37,161,55,78,195,147,88,170,101,162,231,13,162,88,122,30,
    127,124,54,158,164,153,250,101,150,165,153,75,106,229,193,56,
    245,171,21,164,212,32,78,115,217,161,221,120,27,151,200,43,
    154,61,152,48,69,98,128,217,165,197,161,204,131,44,154,40,
    180,150,166,72,179,137,90,135,236,196,77,254,57,54,251,67,
    57,126,204,205,195,92,249,253,88,238,247,79,162,56,220,71,
    57,189,223,116,159,121,221,36,77,39,81,50,244,62,137,198,
    210,235,251,185,12,247,39,83,117,148,38,251,184,46,74,148,
    68,21,197,251,215,40,103,15,167,221,166,109,78,163,161,23,
    177,128,222,145,140,39,50,107,209,232,54,177,32,218,162,41,
    108,97,138,142,104,97,175,134,143,41,30,24,43,226,32,34,
    17,3,18,155,16,102,150,152,250,7,176,245,208,248,199,6,
    100,15,8,49,35,252,9,50,49,226,166,75,223,12,254,246,
    91,210,141,30,29,153,132,3,61,120,198,40,67,184,225,204,
    39,100,248,4,24,42,53,24,217,160,33,132,200,211,152,202,
    166,212,226,116,34,99,32,113,11,242,191,95,166,144,180,1,
    117,143,158,140,67,119,112,171,63,51,42,187,29,98,252,128,
    145,161,142,162,60,61,77,88,255,212,103,63,234,162,78,94,
    76,159,247,71,50,80,249,14,14,124,154,158,56,129,159,36,
    169,114,252,48,116,124,165,178,168,127,162,100,238,168,212,217,
    205,59,117,50,248,90,9,174,138,222,116,82,130,137,12,143,
    96,210,47,97,20,40,124,89,231,23,214,127,46,21,2,227,
    40,13,115,28,39,18,67,169,92,98,82,221,194,230,131,114,
    59,70,96,199,46,241,146,203,120,160,26,12,61,63,207,61,
    222,142,198,25,101,180,250,149,31,159,72,69,243,17,60,10,
    119,165,174,222,104,161,56,187,75,146,150,130,146,242,188,36,
    77,194,41,242,25,5,187,196,194,93,70,91,19,8,111,155,
    136,181,37,108,109,104,33,246,218,70,64,34,89,5,210,24,
    101,119,72,1,192,150,23,69,252,64,196,93,96,148,233,24,
    28,38,88,54,118,66,135,122,180,216,37,64,187,247,169,121,
    64,205,119,74,241,23,165,131,214,85,29,60,162,125,13,22,
    60,48,11,17,43,71,58,184,228,72,247,102,142,132,81,177,
    75,14,97,144,219,204,28,194,36,37,100,79,11,244,147,171,
    33,0,240,243,28,230,89,53,110,155,68,182,75,184,186,132,
    193,121,32,14,231,128,232,146,85,24,133,238,189,155,212,184,
    243,230,212,56,212,106,124,76,251,54,11,252,180,24,55,13,
    17,144,241,141,66,169,172,208,143,176,51,221,34,133,206,171,
    114,11,243,220,97,210,226,132,197,73,143,211,189,142,32,90,
    195,186,99,17,210,6,38,220,41,18,81,78,14,63,201,210,
    47,166,78,58,112,20,148,60,60,217,205,247,118,243,247,49,
    36,56,79,57,200,232,160,160,221,62,147,147,12,221,187,206,
    47,218,101,61,118,95,175,72,35,168,114,74,234,108,41,86,
    52,71,168,92,101,20,152,22,170,229,70,165,101,98,250,125,
    218,180,193,42,54,97,11,159,134,96,206,188,148,35,36,151,
    22,252,21,159,95,144,178,73,94,9,84,240,185,93,205,55,
    139,68,194,185,223,191,132,152,5,9,228,190,141,59,124,84,
    58,156,13,21,62,232,49,137,101,242,129,191,2,23,95,2,
    254,2,132,5,52,121,225,53,236,159,244,144,73,215,105,250,
    31,129,131,207,53,137,206,208,222,103,20,225,9,157,51,127,
    151,167,234,188,247,43,248,219,92,228,186,48,65,80,142,50,
    139,242,106,62,71,89,149,191,50,136,190,85,30,178,46,59,
    54,25,233,200,207,105,154,118,97,179,114,225,89,16,172,106,
    34,140,75,139,194,215,178,222,206,35,206,158,205,208,69,9,
    224,190,88,55,230,48,243,67,106,30,86,112,17,229,216,2,
    152,220,185,26,177,231,178,150,167,163,228,199,196,137,197,188,
    175,218,156,230,136,74,247,3,239,195,231,191,126,126,208,245,
    102,4,43,39,169,149,78,242,168,114,18,201,97,252,53,151,
    227,212,26,132,134,11,67,224,249,7,75,23,58,122,88,32,
    107,208,179,201,157,184,212,20,133,183,137,50,188,81,88,188,
    148,35,88,83,7,90,135,21,32,180,173,169,249,98,161,225,
    132,204,253,36,246,199,253,208,127,250,123,218,146,246,13,74,
    255,51,74,33,218,243,66,144,239,136,155,228,224,215,31,149,
    194,188,90,104,40,249,49,238,80,9,193,142,19,166,1,199,
    143,79,142,164,51,150,227,62,158,194,142,162,137,51,136,253,
    33,91,202,44,132,124,94,10,169,216,212,87,115,115,78,65,
    234,32,117,130,52,193,152,127,18,168,52,115,66,137,7,19,
    25,58,15,29,78,24,78,148,59,126,31,191,250,129,210,190,
    112,217,173,185,252,243,179,97,206,149,222,241,41,117,23,110,
    105,15,207,159,17,22,184,135,165,146,244,161,168,138,254,92,
    211,106,199,194,20,138,7,14,53,213,97,238,29,106,126,64,
    205,46,188,137,36,177,143,59,252,142,182,34,229,217,24,137,
    234,130,107,163,75,19,95,208,218,252,171,238,124,250,109,220,
    89,95,100,20,78,109,211,76,185,68,231,96,106,235,148,43,
    122,141,114,112,133,219,38,15,182,202,187,146,91,60,184,10,
    189,54,93,56,208,200,26,197,132,165,255,53,38,176,47,45,
    220,139,62,255,191,134,2,247,209,27,149,193,253,9,20,101,
    196,77,97,64,204,11,216,210,97,96,36,202,122,125,94,58,
    190,17,216,190,30,121,94,144,73,95,73,109,182,237,5,139,
    204,97,69,51,224,85,198,43,229,170,14,47,239,86,178,93,
    112,33,53,221,152,43,168,217,150,226,16,171,30,44,186,207,
    88,124,207,208,117,247,12,162,86,165,133,45,108,18,121,234,
    93,163,9,93,91,19,71,254,100,34,147,112,86,55,243,151,
    133,66,129,66,87,31,102,133,12,22,201,27,248,124,213,41,
    73,178,57,65,217,138,181,202,13,23,109,79,134,240,176,180,
    100,135,238,131,102,113,218,37,59,234,200,92,5,101,247,103,
    149,101,222,186,1,159,161,124,21,5,146,142,103,223,52,5,
    203,42,54,159,126,189,113,118,114,66,190,244,181,4,139,41,
    37,65,253,202,70,97,55,10,101,44,17,177,215,172,84,36,
    94,113,246,13,37,38,214,116,138,135,50,62,220,224,123,236,
    121,139,207,66,239,225,14,159,65,17,3,41,11,217,152,135,
    54,241,191,110,213,5,167,250,43,87,187,154,59,42,232,117,
    25,63,205,93,14,72,171,149,169,248,250,177,76,181,236,157,
    116,8,61,240,199,250,70,137,111,73,220,239,82,243,189,210,
    218,172,71,125,190,227,195,147,62,182,162,39,114,29,194,101,
    135,187,71,227,116,23,48,126,188,87,74,181,167,165,234,70,
    99,125,125,198,55,160,227,199,106,237,202,180,48,243,177,191,
    121,101,52,151,89,228,199,209,151,250,218,172,28,86,36,201,
    85,186,196,79,245,198,169,91,237,194,205,245,55,155,56,147,
    195,40,71,154,76,176,90,92,68,19,210,187,114,110,0,216,
    252,234,133,67,66,215,207,250,142,224,41,221,30,231,79,176,
    161,187,178,250,106,93,216,6,221,202,154,162,33,90,194,18,
    205,86,221,172,219,245,154,137,176,161,145,117,209,48,235,141,
    166,160,255,29,4,80,195,216,105,214,197,127,1,164,208,201,
    198,
};

EmbeddedPython embedded_m5_internal_param_X86IntSinkPin(
    "m5/internal/param_X86IntSinkPin.py",
    "/gem5/gem5-stable/build/X86_MSI_Snooping_Time_based/python/m5/internal/param_X86IntSinkPin.py",
    "m5.internal.param_X86IntSinkPin",
    data_m5_internal_param_X86IntSinkPin,
    2145,
    6573);

} // anonymous namespace
