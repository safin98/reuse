#ifndef __PARAMS__L1Cache_Controller__
#define __PARAMS__L1Cache_Controller__

class L1Cache_Controller;

#include <cstddef>
#include "params/RubyCache.hh"
#include <cstddef>
#include "params/RubyCache.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include <cstddef>
#include "params/RubySequencer.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"

#include "params/RubyController.hh"

struct L1Cache_ControllerParams
    : public RubyControllerParams
{
    L1Cache_Controller * create();
    CacheMemory * Dcache;
    CacheMemory * Icache;
    int is_blocked;
    Cycles l1_request_latency;
    Cycles l1_response_latency;
    Cycles max_S_to_M_latency;
    Cycles max_arbit_latency;
    Cycles max_interC_latency;
    Cycles max_intraC_latency;
    Cycles max_wcet_latency;
    bool send_evictions;
    Sequencer * sequencer;
    Cycles timeout_0;
    Cycles timeout_latency_CC;
    Cycles timeout_latency_CN;
    Cycles timeout_latency_NC;
    Cycles timeout_latency_NN;
    Cycles total_latency;
    unsigned int port_requestFromCache_connection_count;
    unsigned int port_responseToCache_connection_count;
    unsigned int port_atomicRequestToCache_connection_count;
    unsigned int port_responseFromCache_connection_count;
    unsigned int port_atomicRequestFromCache_connection_count;
    unsigned int port_requestToCache_connection_count;
    unsigned int port_requestToCacheWB_connection_count;
    unsigned int port_requestFromCacheWB_connection_count;
};

#endif // __PARAMS__L1Cache_Controller__
