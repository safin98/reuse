#include "sim/init.hh"

namespace {

const uint8_t data_m5_objects_GarnetNetwork_d[] = {
    120,156,189,84,75,111,211,64,16,30,59,206,187,208,82,80,
    133,184,224,3,72,81,165,214,82,171,74,32,85,8,149,74,
    168,151,22,185,112,32,23,203,177,199,181,83,63,34,239,38,
    36,231,114,226,79,195,204,218,78,210,36,18,61,64,227,120,
    52,243,237,238,236,60,62,143,7,229,175,70,239,71,19,64,
    252,34,197,167,191,6,49,192,215,82,211,10,77,135,88,135,
    68,135,190,14,26,219,53,136,107,144,212,160,95,43,108,3,
    98,3,18,3,250,6,217,117,192,26,4,26,248,13,248,9,
    112,7,240,189,95,7,191,9,104,40,180,53,71,27,224,183,
    1,117,133,118,230,104,19,252,46,92,247,182,40,156,232,55,
    253,122,26,105,146,197,126,161,62,35,113,230,10,252,236,230,
    41,202,75,148,63,178,252,182,88,234,22,75,145,103,103,99,
    137,121,1,62,33,241,41,206,188,91,244,175,6,67,244,164,
    124,74,72,113,186,216,231,248,222,114,57,206,184,28,231,164,
    32,64,95,227,162,80,222,84,13,202,150,42,65,57,98,29,
    134,13,192,38,12,91,92,139,59,2,91,75,96,155,11,194,
    96,7,236,235,94,157,28,217,6,7,210,98,173,188,80,28,
    145,145,96,98,229,227,193,204,74,139,36,172,27,21,148,21,
    68,83,244,15,70,209,8,227,40,69,171,58,115,24,134,226,
    13,29,155,68,185,28,187,177,233,133,110,154,98,44,204,17,
    230,102,5,150,174,196,43,218,152,142,147,1,45,101,193,218,
    106,175,91,5,228,56,169,155,160,227,200,142,50,146,204,31,
    199,108,170,136,103,35,148,109,82,188,233,212,241,98,87,8,
    181,139,173,16,93,31,115,201,201,125,113,115,55,145,13,210,
    190,93,164,242,248,72,169,4,98,42,37,119,113,226,9,135,
    34,116,38,116,185,92,132,229,100,129,83,134,229,84,97,169,
    203,24,100,68,244,184,33,11,33,174,72,88,55,152,156,40,
    113,32,164,59,136,209,18,185,103,61,176,144,247,24,67,245,
    28,205,84,99,222,178,115,230,68,67,43,158,45,173,77,175,
    124,57,167,73,121,132,210,195,60,112,61,220,68,152,235,191,
    18,134,169,66,96,179,162,74,107,137,42,237,37,176,3,244,
    13,49,216,101,254,112,53,109,230,188,220,37,177,30,137,56,
    125,56,147,214,79,51,167,246,201,193,197,185,25,165,102,142,
    177,43,163,44,53,101,102,102,50,36,238,148,254,104,177,60,
    34,254,25,5,153,27,182,202,174,201,66,125,29,220,127,155,
    57,102,51,65,109,222,34,117,158,4,190,205,159,177,205,93,
    178,183,89,236,220,227,198,255,35,8,87,254,144,157,239,44,
    8,162,111,87,20,217,94,165,200,38,102,188,219,200,12,26,
    135,195,58,243,163,156,32,141,10,49,170,246,55,231,237,127,
    193,1,188,127,120,159,87,243,8,195,136,137,46,40,26,24,
    140,131,0,243,162,99,190,43,93,115,165,151,145,182,105,167,
    39,243,120,117,103,175,181,161,131,43,205,123,190,240,163,134,
    0,223,232,76,188,53,156,253,19,254,24,29,229,98,114,45,
    69,107,222,209,93,237,82,37,163,230,79,114,114,56,226,145,
    38,212,124,100,43,207,166,51,155,203,98,51,23,237,90,53,
    209,21,57,148,191,71,136,91,69,115,90,204,231,15,175,171,
    145,213,209,58,218,14,63,250,94,123,175,251,7,183,144,235,
    144,
};

EmbeddedPython embedded_m5_objects_GarnetNetwork_d(
    "m5/objects/GarnetNetwork_d.py",
    "/gem5/gem5-stable/src/mem/ruby/network/garnet/fixed-pipeline/GarnetNetwork_d.py",
    "m5.objects.GarnetNetwork_d",
    data_m5_objects_GarnetNetwork_d,
    753,
    2006);

} // anonymous namespace
