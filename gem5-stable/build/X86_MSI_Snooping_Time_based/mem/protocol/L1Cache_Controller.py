from m5.params import *
from m5.SimObject import SimObject
from Controller import RubyController

class L1Cache_Controller(RubyController):
    type = 'L1Cache_Controller'
    cxx_header = 'mem/protocol/L1Cache_Controller.hh'
    sequencer = Param.RubySequencer("")
    Icache = Param.RubyCache("")
    Dcache = Param.RubyCache("")
    l1_request_latency = Param.Cycles((5), "")
    l1_response_latency = Param.Cycles((2), "")
    send_evictions = Param.Bool("")
    is_blocked = Param.Int("")
    max_wcet_latency = Param.Cycles((0), "")
    max_arbit_latency = Param.Cycles((0), "")
    max_interC_latency = Param.Cycles((0), "")
    max_intraC_latency = Param.Cycles((0), "")
    max_S_to_M_latency = Param.Cycles((0), "")
    total_latency = Param.Cycles((0), "")
    timeout_latency_CC = Param.Cycles("")
    timeout_latency_CN = Param.Cycles("")
    timeout_latency_NC = Param.Cycles("")
    timeout_latency_NN = Param.Cycles("")
    timeout_0 = Param.Cycles((0), "")
    requestFromCache = MasterPort("")
    requestFromCacheWB = MasterPort("")
    responseFromCache = MasterPort("")
    atomicRequestFromCache = MasterPort("")
    responseToCache = SlavePort("")
    requestToCache = SlavePort("")
    requestToCacheWB = SlavePort("")
    atomicRequestToCache = SlavePort("")
