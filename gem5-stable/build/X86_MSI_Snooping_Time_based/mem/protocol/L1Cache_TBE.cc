/** \file L1Cache_TBE.cc
 *
 * Auto generated C++ code started by /gem5/gem5-stable/src/mem/slicc/symbols/Type.py:410
 */

#include <iostream>
#include <memory>

#include "mem/protocol/L1Cache_TBE.hh"
#include "mem/ruby/common/Global.hh"
#include "mem/ruby/system/System.hh"

using namespace std;
/** \brief Print the state of this object */
void
L1Cache_TBE::print(ostream& out) const
{
    out << "[L1Cache_TBE: ";
    out << "Addr = " << m_Addr << " ";
    out << "TBEState = " << m_TBEState << " ";
    out << "DataBlk = " << m_DataBlk << " ";
    out << "Dirty = " << m_Dirty << " ";
    out << "pendingAcks = " << m_pendingAcks << " ";
    out << "isAtomic = " << m_isAtomic << " ";
    out << "startTime = " << m_startTime << " ";
    out << "endTime = " << m_endTime << " ";
    out << "arbitLatency = " << m_arbitLatency << " ";
    out << "interCoreCohLatency = " << m_interCoreCohLatency << " ";
    out << "intraCoreCohLatency = " << m_intraCoreCohLatency << " ";
    out << "firstScheduledTime = " << m_firstScheduledTime << " ";
    out << "CoreID = " << m_CoreID << " ";
    out << "zeroArb = " << m_zeroArb << " ";
    out << "startTime_SM = " << m_startTime_SM << " ";
    out << "]";
}
