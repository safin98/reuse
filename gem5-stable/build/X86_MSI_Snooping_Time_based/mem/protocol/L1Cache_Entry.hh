/** \file L1Cache_Entry.hh
 *
 *
 * Auto generated C++ code started by /gem5/gem5-stable/src/mem/slicc/symbols/Type.py:200
 */

#ifndef __L1Cache_Entry_HH__
#define __L1Cache_Entry_HH__

#include <iostream>

#include "mem/ruby/slicc_interface/RubySlicc_Util.hh"
#include "mem/protocol/L1Cache_State.hh"
#include "mem/protocol/DataBlock.hh"
#include "mem/protocol/MachineID.hh"
#include "mem/protocol/NetDest.hh"
#include "mem/protocol/NetDest.hh"
#include "mem/protocol/AbstractCacheEntry.hh"
class L1Cache_Entry :  public AbstractCacheEntry
{
  public:
    L1Cache_Entry
()
		{
        m_CacheState = L1Cache_State_I; // default value of L1Cache_State
        // m_DataBlk has no default
        m_Dirty = false; // default for this field
        // m_finalDestination has no default
        // m_finalDestinationSet_NC has no default
        // m_finalDestinationSet_C has no default
        m_Timeout = Cycles(0); // default value of Cycles
        m_isAtomic = false; // default for this field
        m_startTime = Cycles(0); // default for this field
        m_endTime = Cycles(0); // default for this field
        m_arbitLatency = Cycles(0); // default for this field
        m_interCoreCohLatency = Cycles(0); // default for this field
        m_intraCoreCohLatency = Cycles(0); // default for this field
        m_firstScheduledTime = Cycles(0); // default for this field
        m_CoreID = 0; // default value of int
        m_zeroArb = false; // default for this field
    }
    L1Cache_Entry(const L1Cache_Entry&other)
        : AbstractCacheEntry(other)
    {
        m_CacheState = other.m_CacheState;
        m_DataBlk = other.m_DataBlk;
        m_Dirty = other.m_Dirty;
        m_finalDestination = other.m_finalDestination;
        m_finalDestinationSet_NC = other.m_finalDestinationSet_NC;
        m_finalDestinationSet_C = other.m_finalDestinationSet_C;
        m_Timeout = other.m_Timeout;
        m_isAtomic = other.m_isAtomic;
        m_startTime = other.m_startTime;
        m_endTime = other.m_endTime;
        m_arbitLatency = other.m_arbitLatency;
        m_interCoreCohLatency = other.m_interCoreCohLatency;
        m_intraCoreCohLatency = other.m_intraCoreCohLatency;
        m_firstScheduledTime = other.m_firstScheduledTime;
        m_CoreID = other.m_CoreID;
        m_zeroArb = other.m_zeroArb;
    }
    L1Cache_Entry(const L1Cache_State& local_CacheState, const DataBlock& local_DataBlk, const bool& local_Dirty, const MachineID& local_finalDestination, const NetDest& local_finalDestinationSet_NC, const NetDest& local_finalDestinationSet_C, const Cycles& local_Timeout, const bool& local_isAtomic, const Cycles& local_startTime, const Cycles& local_endTime, const Cycles& local_arbitLatency, const Cycles& local_interCoreCohLatency, const Cycles& local_intraCoreCohLatency, const Cycles& local_firstScheduledTime, const int& local_CoreID, const bool& local_zeroArb)
        : AbstractCacheEntry()
    {
        m_CacheState = local_CacheState;
        m_DataBlk = local_DataBlk;
        m_Dirty = local_Dirty;
        m_finalDestination = local_finalDestination;
        m_finalDestinationSet_NC = local_finalDestinationSet_NC;
        m_finalDestinationSet_C = local_finalDestinationSet_C;
        m_Timeout = local_Timeout;
        m_isAtomic = local_isAtomic;
        m_startTime = local_startTime;
        m_endTime = local_endTime;
        m_arbitLatency = local_arbitLatency;
        m_interCoreCohLatency = local_interCoreCohLatency;
        m_intraCoreCohLatency = local_intraCoreCohLatency;
        m_firstScheduledTime = local_firstScheduledTime;
        m_CoreID = local_CoreID;
        m_zeroArb = local_zeroArb;
    }
    L1Cache_Entry*
    clone() const
    {
         return new L1Cache_Entry(*this);
    }
    // Const accessors methods for each field
    /** \brief Const accessor method for CacheState field.
     *  \return CacheState field
     */
    const L1Cache_State&
    getCacheState() const
    {
        return m_CacheState;
    }
    /** \brief Const accessor method for DataBlk field.
     *  \return DataBlk field
     */
    const DataBlock&
    getDataBlk() const
    {
        return m_DataBlk;
    }
    /** \brief Const accessor method for Dirty field.
     *  \return Dirty field
     */
    const bool&
    getDirty() const
    {
        return m_Dirty;
    }
    /** \brief Const accessor method for finalDestination field.
     *  \return finalDestination field
     */
    const MachineID&
    getfinalDestination() const
    {
        return m_finalDestination;
    }
    /** \brief Const accessor method for finalDestinationSet_NC field.
     *  \return finalDestinationSet_NC field
     */
    const NetDest&
    getfinalDestinationSet_NC() const
    {
        return m_finalDestinationSet_NC;
    }
    /** \brief Const accessor method for finalDestinationSet_C field.
     *  \return finalDestinationSet_C field
     */
    const NetDest&
    getfinalDestinationSet_C() const
    {
        return m_finalDestinationSet_C;
    }
    /** \brief Const accessor method for Timeout field.
     *  \return Timeout field
     */
    const Cycles&
    getTimeout() const
    {
        return m_Timeout;
    }
    /** \brief Const accessor method for isAtomic field.
     *  \return isAtomic field
     */
    const bool&
    getisAtomic() const
    {
        return m_isAtomic;
    }
    /** \brief Const accessor method for startTime field.
     *  \return startTime field
     */
    const Cycles&
    getstartTime() const
    {
        return m_startTime;
    }
    /** \brief Const accessor method for endTime field.
     *  \return endTime field
     */
    const Cycles&
    getendTime() const
    {
        return m_endTime;
    }
    /** \brief Const accessor method for arbitLatency field.
     *  \return arbitLatency field
     */
    const Cycles&
    getarbitLatency() const
    {
        return m_arbitLatency;
    }
    /** \brief Const accessor method for interCoreCohLatency field.
     *  \return interCoreCohLatency field
     */
    const Cycles&
    getinterCoreCohLatency() const
    {
        return m_interCoreCohLatency;
    }
    /** \brief Const accessor method for intraCoreCohLatency field.
     *  \return intraCoreCohLatency field
     */
    const Cycles&
    getintraCoreCohLatency() const
    {
        return m_intraCoreCohLatency;
    }
    /** \brief Const accessor method for firstScheduledTime field.
     *  \return firstScheduledTime field
     */
    const Cycles&
    getfirstScheduledTime() const
    {
        return m_firstScheduledTime;
    }
    /** \brief Const accessor method for CoreID field.
     *  \return CoreID field
     */
    const int&
    getCoreID() const
    {
        return m_CoreID;
    }
    /** \brief Const accessor method for zeroArb field.
     *  \return zeroArb field
     */
    const bool&
    getzeroArb() const
    {
        return m_zeroArb;
    }
    // Non const Accessors methods for each field
    /** \brief Non-const accessor method for CacheState field.
     *  \return CacheState field
     */
    L1Cache_State&
    getCacheState()
    {
        return m_CacheState;
    }
    /** \brief Non-const accessor method for DataBlk field.
     *  \return DataBlk field
     */
    DataBlock&
    getDataBlk()
    {
        return m_DataBlk;
    }
    /** \brief Non-const accessor method for Dirty field.
     *  \return Dirty field
     */
    bool&
    getDirty()
    {
        return m_Dirty;
    }
    /** \brief Non-const accessor method for finalDestination field.
     *  \return finalDestination field
     */
    MachineID&
    getfinalDestination()
    {
        return m_finalDestination;
    }
    /** \brief Non-const accessor method for finalDestinationSet_NC field.
     *  \return finalDestinationSet_NC field
     */
    NetDest&
    getfinalDestinationSet_NC()
    {
        return m_finalDestinationSet_NC;
    }
    /** \brief Non-const accessor method for finalDestinationSet_C field.
     *  \return finalDestinationSet_C field
     */
    NetDest&
    getfinalDestinationSet_C()
    {
        return m_finalDestinationSet_C;
    }
    /** \brief Non-const accessor method for Timeout field.
     *  \return Timeout field
     */
    Cycles&
    getTimeout()
    {
        return m_Timeout;
    }
    /** \brief Non-const accessor method for isAtomic field.
     *  \return isAtomic field
     */
    bool&
    getisAtomic()
    {
        return m_isAtomic;
    }
    /** \brief Non-const accessor method for startTime field.
     *  \return startTime field
     */
    Cycles&
    getstartTime()
    {
        return m_startTime;
    }
    /** \brief Non-const accessor method for endTime field.
     *  \return endTime field
     */
    Cycles&
    getendTime()
    {
        return m_endTime;
    }
    /** \brief Non-const accessor method for arbitLatency field.
     *  \return arbitLatency field
     */
    Cycles&
    getarbitLatency()
    {
        return m_arbitLatency;
    }
    /** \brief Non-const accessor method for interCoreCohLatency field.
     *  \return interCoreCohLatency field
     */
    Cycles&
    getinterCoreCohLatency()
    {
        return m_interCoreCohLatency;
    }
    /** \brief Non-const accessor method for intraCoreCohLatency field.
     *  \return intraCoreCohLatency field
     */
    Cycles&
    getintraCoreCohLatency()
    {
        return m_intraCoreCohLatency;
    }
    /** \brief Non-const accessor method for firstScheduledTime field.
     *  \return firstScheduledTime field
     */
    Cycles&
    getfirstScheduledTime()
    {
        return m_firstScheduledTime;
    }
    /** \brief Non-const accessor method for CoreID field.
     *  \return CoreID field
     */
    int&
    getCoreID()
    {
        return m_CoreID;
    }
    /** \brief Non-const accessor method for zeroArb field.
     *  \return zeroArb field
     */
    bool&
    getzeroArb()
    {
        return m_zeroArb;
    }
    // Mutator methods for each field
    /** \brief Mutator method for CacheState field */
    void
    setCacheState(const L1Cache_State& local_CacheState)
    {
        m_CacheState = local_CacheState;
    }
    /** \brief Mutator method for DataBlk field */
    void
    setDataBlk(const DataBlock& local_DataBlk)
    {
        m_DataBlk = local_DataBlk;
    }
    /** \brief Mutator method for Dirty field */
    void
    setDirty(const bool& local_Dirty)
    {
        m_Dirty = local_Dirty;
    }
    /** \brief Mutator method for finalDestination field */
    void
    setfinalDestination(const MachineID& local_finalDestination)
    {
        m_finalDestination = local_finalDestination;
    }
    /** \brief Mutator method for finalDestinationSet_NC field */
    void
    setfinalDestinationSet_NC(const NetDest& local_finalDestinationSet_NC)
    {
        m_finalDestinationSet_NC = local_finalDestinationSet_NC;
    }
    /** \brief Mutator method for finalDestinationSet_C field */
    void
    setfinalDestinationSet_C(const NetDest& local_finalDestinationSet_C)
    {
        m_finalDestinationSet_C = local_finalDestinationSet_C;
    }
    /** \brief Mutator method for Timeout field */
    void
    setTimeout(const Cycles& local_Timeout)
    {
        m_Timeout = local_Timeout;
    }
    /** \brief Mutator method for isAtomic field */
    void
    setisAtomic(const bool& local_isAtomic)
    {
        m_isAtomic = local_isAtomic;
    }
    /** \brief Mutator method for startTime field */
    void
    setstartTime(const Cycles& local_startTime)
    {
        m_startTime = local_startTime;
    }
    /** \brief Mutator method for endTime field */
    void
    setendTime(const Cycles& local_endTime)
    {
        m_endTime = local_endTime;
    }
    /** \brief Mutator method for arbitLatency field */
    void
    setarbitLatency(const Cycles& local_arbitLatency)
    {
        m_arbitLatency = local_arbitLatency;
    }
    /** \brief Mutator method for interCoreCohLatency field */
    void
    setinterCoreCohLatency(const Cycles& local_interCoreCohLatency)
    {
        m_interCoreCohLatency = local_interCoreCohLatency;
    }
    /** \brief Mutator method for intraCoreCohLatency field */
    void
    setintraCoreCohLatency(const Cycles& local_intraCoreCohLatency)
    {
        m_intraCoreCohLatency = local_intraCoreCohLatency;
    }
    /** \brief Mutator method for firstScheduledTime field */
    void
    setfirstScheduledTime(const Cycles& local_firstScheduledTime)
    {
        m_firstScheduledTime = local_firstScheduledTime;
    }
    /** \brief Mutator method for CoreID field */
    void
    setCoreID(const int& local_CoreID)
    {
        m_CoreID = local_CoreID;
    }
    /** \brief Mutator method for zeroArb field */
    void
    setzeroArb(const bool& local_zeroArb)
    {
        m_zeroArb = local_zeroArb;
    }
    void print(std::ostream& out) const;
  //private:
    /** cache state */
    L1Cache_State m_CacheState;
    /** data for the block */
    DataBlock m_DataBlk;
    /** data is dirty */
    bool m_Dirty;
    /** where this cache block should go in cache-to-cache transfer */
    MachineID m_finalDestination;
    /** where this cache block should go in cache-to-cache transfer */
    NetDest m_finalDestinationSet_NC;
    /** where this cache block should go in cache-to-cache transfer */
    NetDest m_finalDestinationSet_C;
    /** timeout when this should invalidate */
    Cycles m_Timeout;
    /** data is atomic */
    bool m_isAtomic;
    /** start time of request */
    Cycles m_startTime;
    /** end time of request */
    Cycles m_endTime;
    /** arbit latency of request */
    Cycles m_arbitLatency;
    /** inter-core coherence latency of request */
    Cycles m_interCoreCohLatency;
    /** intra-core coherence latency of request */
    Cycles m_intraCoreCohLatency;
    /** first scheduled time */
    Cycles m_firstScheduledTime;
    int m_CoreID;
    /** zero arbitration */
    bool m_zeroArb;
};
inline std::ostream&
operator<<(std::ostream& out, const L1Cache_Entry& obj)
{
    obj.print(out);
    out << std::flush;
    return out;
}

#endif // __L1Cache_Entry_HH__
