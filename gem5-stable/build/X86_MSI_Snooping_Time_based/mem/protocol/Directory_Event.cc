/** \file Directory_Event.hh
 *
 * Auto generated C++ code started by /gem5/gem5-stable/src/mem/slicc/symbols/Type.py:550
 */

#include <cassert>
#include <iostream>
#include <string>

#include "base/misc.hh"
#include "mem/protocol/Directory_Event.hh"

using namespace std;

// Code for output operator
ostream&
operator<<(ostream& out, const Directory_Event& obj)
{
    out << Directory_Event_to_string(obj);
    out << flush;
    return out;
}

// Code to convert state to a string
string
Directory_Event_to_string(const Directory_Event& obj)
{
    switch(obj) {
      case Directory_Event_Fetch:
        return "Fetch";
      case Directory_Event_Data:
        return "Data";
      case Directory_Event_Memory_Data:
        return "Memory_Data";
      case Directory_Event_Memory_Ack:
        return "Memory_Ack";
      case Directory_Event_DMA_READ:
        return "DMA_READ";
      case Directory_Event_DMA_WRITE:
        return "DMA_WRITE";
      case Directory_Event_GETS:
        return "GETS";
      case Directory_Event_PUTM:
        return "PUTM";
      case Directory_Event_GETM:
        return "GETM";
      case Directory_Event_GETMO:
        return "GETMO";
      case Directory_Event_UPG:
        return "UPG";
      case Directory_Event_GETSD:
        return "GETSD";
      case Directory_Event_PUTMD:
        return "PUTMD";
      case Directory_Event_GETMD:
        return "GETMD";
      case Directory_Event_GETMOD:
        return "GETMOD";
      case Directory_Event_UPGD:
        return "UPGD";
      case Directory_Event_InvLatest:
        return "InvLatest";
      case Directory_Event_GETM_C:
        return "GETM_C";
      case Directory_Event_GETM_NC:
        return "GETM_NC";
      case Directory_Event_GETS_C:
        return "GETS_C";
      case Directory_Event_GETS_NC:
        return "GETS_NC";
      case Directory_Event_SENDDATA:
        return "SENDDATA";
      case Directory_Event_NoData:
        return "NoData";
      case Directory_Event_WaitForData:
        return "WaitForData";
      case Directory_Event_DATAACK:
        return "DATAACK";
      default:
        panic("Invalid range for type Directory_Event");
    }
}

// Code to convert from a string to the enumeration
Directory_Event
string_to_Directory_Event(const string& str)
{
    if (str == "Fetch") {
        return Directory_Event_Fetch;
    } else if (str == "Data") {
        return Directory_Event_Data;
    } else if (str == "Memory_Data") {
        return Directory_Event_Memory_Data;
    } else if (str == "Memory_Ack") {
        return Directory_Event_Memory_Ack;
    } else if (str == "DMA_READ") {
        return Directory_Event_DMA_READ;
    } else if (str == "DMA_WRITE") {
        return Directory_Event_DMA_WRITE;
    } else if (str == "GETS") {
        return Directory_Event_GETS;
    } else if (str == "PUTM") {
        return Directory_Event_PUTM;
    } else if (str == "GETM") {
        return Directory_Event_GETM;
    } else if (str == "GETMO") {
        return Directory_Event_GETMO;
    } else if (str == "UPG") {
        return Directory_Event_UPG;
    } else if (str == "GETSD") {
        return Directory_Event_GETSD;
    } else if (str == "PUTMD") {
        return Directory_Event_PUTMD;
    } else if (str == "GETMD") {
        return Directory_Event_GETMD;
    } else if (str == "GETMOD") {
        return Directory_Event_GETMOD;
    } else if (str == "UPGD") {
        return Directory_Event_UPGD;
    } else if (str == "InvLatest") {
        return Directory_Event_InvLatest;
    } else if (str == "GETM_C") {
        return Directory_Event_GETM_C;
    } else if (str == "GETM_NC") {
        return Directory_Event_GETM_NC;
    } else if (str == "GETS_C") {
        return Directory_Event_GETS_C;
    } else if (str == "GETS_NC") {
        return Directory_Event_GETS_NC;
    } else if (str == "SENDDATA") {
        return Directory_Event_SENDDATA;
    } else if (str == "NoData") {
        return Directory_Event_NoData;
    } else if (str == "WaitForData") {
        return Directory_Event_WaitForData;
    } else if (str == "DATAACK") {
        return Directory_Event_DATAACK;
    } else {
        panic("Invalid string conversion for %s, type Directory_Event", str);
    }
}

// Code to increment an enumeration type
Directory_Event&
operator++(Directory_Event& e)
{
    assert(e < Directory_Event_NUM);
    return e = Directory_Event(e+1);
}
