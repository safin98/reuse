/** \file L1Cache_Event.hh
 *
 * Auto generated C++ code started by /gem5/gem5-stable/src/mem/slicc/symbols/Type.py:550
 */

#include <cassert>
#include <iostream>
#include <string>

#include "base/misc.hh"
#include "mem/protocol/L1Cache_Event.hh"

using namespace std;

// Code for output operator
ostream&
operator<<(ostream& out, const L1Cache_Event& obj)
{
    out << L1Cache_Event_to_string(obj);
    out << flush;
    return out;
}

// Code to convert state to a string
string
L1Cache_Event_to_string(const L1Cache_Event& obj)
{
    switch(obj) {
      case L1Cache_Event_Load:
        return "Load";
      case L1Cache_Event_Store:
        return "Store";
      case L1Cache_Event_Ifetch:
        return "Ifetch";
      case L1Cache_Event_RMW_Read:
        return "RMW_Read";
      case L1Cache_Event_RMW_Write:
        return "RMW_Write";
      case L1Cache_Event_Replacement:
        return "Replacement";
      case L1Cache_Event_Other_GETS_C:
        return "Other_GETS_C";
      case L1Cache_Event_Other_GETS_NC:
        return "Other_GETS_NC";
      case L1Cache_Event_Other_GETM_C:
        return "Other_GETM_C";
      case L1Cache_Event_Other_GETM_NC:
        return "Other_GETM_NC";
      case L1Cache_Event_Other_GETI_C:
        return "Other_GETI_C";
      case L1Cache_Event_Other_GETI_NC:
        return "Other_GETI_NC";
      case L1Cache_Event_Other_PUTM:
        return "Other_PUTM";
      case L1Cache_Event_OWN_GETS:
        return "OWN_GETS";
      case L1Cache_Event_OWN_GETM:
        return "OWN_GETM";
      case L1Cache_Event_OWN_GETI:
        return "OWN_GETI";
      case L1Cache_Event_OWN_PUTM:
        return "OWN_PUTM";
      case L1Cache_Event_INV:
        return "INV";
      case L1Cache_Event_Data:
        return "Data";
      case L1Cache_Event_Ack:
        return "Ack";
      case L1Cache_Event_Timeout_noinv:
        return "Timeout_noinv";
      case L1Cache_Event_Timeout_noinv_noncric:
        return "Timeout_noinv_noncric";
      case L1Cache_Event_Timeout_CC:
        return "Timeout_CC";
      case L1Cache_Event_Timeout_CN:
        return "Timeout_CN";
      case L1Cache_Event_Timeout_NC:
        return "Timeout_NC";
      case L1Cache_Event_Timeout_NN:
        return "Timeout_NN";
      case L1Cache_Event_Own_SENDDATA:
        return "Own_SENDDATA";
      case L1Cache_Event_Other_SENDDATA:
        return "Other_SENDDATA";
      case L1Cache_Event_Own_SELFINV:
        return "Own_SELFINV";
      case L1Cache_Event_Other_SELFINV:
        return "Other_SELFINV";
      case L1Cache_Event_Own_SELFINV_SPL:
        return "Own_SELFINV_SPL";
      case L1Cache_Event_ALLINV:
        return "ALLINV";
      default:
        panic("Invalid range for type L1Cache_Event");
    }
}

// Code to convert from a string to the enumeration
L1Cache_Event
string_to_L1Cache_Event(const string& str)
{
    if (str == "Load") {
        return L1Cache_Event_Load;
    } else if (str == "Store") {
        return L1Cache_Event_Store;
    } else if (str == "Ifetch") {
        return L1Cache_Event_Ifetch;
    } else if (str == "RMW_Read") {
        return L1Cache_Event_RMW_Read;
    } else if (str == "RMW_Write") {
        return L1Cache_Event_RMW_Write;
    } else if (str == "Replacement") {
        return L1Cache_Event_Replacement;
    } else if (str == "Other_GETS_C") {
        return L1Cache_Event_Other_GETS_C;
    } else if (str == "Other_GETS_NC") {
        return L1Cache_Event_Other_GETS_NC;
    } else if (str == "Other_GETM_C") {
        return L1Cache_Event_Other_GETM_C;
    } else if (str == "Other_GETM_NC") {
        return L1Cache_Event_Other_GETM_NC;
    } else if (str == "Other_GETI_C") {
        return L1Cache_Event_Other_GETI_C;
    } else if (str == "Other_GETI_NC") {
        return L1Cache_Event_Other_GETI_NC;
    } else if (str == "Other_PUTM") {
        return L1Cache_Event_Other_PUTM;
    } else if (str == "OWN_GETS") {
        return L1Cache_Event_OWN_GETS;
    } else if (str == "OWN_GETM") {
        return L1Cache_Event_OWN_GETM;
    } else if (str == "OWN_GETI") {
        return L1Cache_Event_OWN_GETI;
    } else if (str == "OWN_PUTM") {
        return L1Cache_Event_OWN_PUTM;
    } else if (str == "INV") {
        return L1Cache_Event_INV;
    } else if (str == "Data") {
        return L1Cache_Event_Data;
    } else if (str == "Ack") {
        return L1Cache_Event_Ack;
    } else if (str == "Timeout_noinv") {
        return L1Cache_Event_Timeout_noinv;
    } else if (str == "Timeout_noinv_noncric") {
        return L1Cache_Event_Timeout_noinv_noncric;
    } else if (str == "Timeout_CC") {
        return L1Cache_Event_Timeout_CC;
    } else if (str == "Timeout_CN") {
        return L1Cache_Event_Timeout_CN;
    } else if (str == "Timeout_NC") {
        return L1Cache_Event_Timeout_NC;
    } else if (str == "Timeout_NN") {
        return L1Cache_Event_Timeout_NN;
    } else if (str == "Own_SENDDATA") {
        return L1Cache_Event_Own_SENDDATA;
    } else if (str == "Other_SENDDATA") {
        return L1Cache_Event_Other_SENDDATA;
    } else if (str == "Own_SELFINV") {
        return L1Cache_Event_Own_SELFINV;
    } else if (str == "Other_SELFINV") {
        return L1Cache_Event_Other_SELFINV;
    } else if (str == "Own_SELFINV_SPL") {
        return L1Cache_Event_Own_SELFINV_SPL;
    } else if (str == "ALLINV") {
        return L1Cache_Event_ALLINV;
    } else {
        panic("Invalid string conversion for %s, type L1Cache_Event", str);
    }
}

// Code to increment an enumeration type
L1Cache_Event&
operator++(L1Cache_Event& e)
{
    assert(e < L1Cache_Event_NUM);
    return e = L1Cache_Event(e+1);
}
