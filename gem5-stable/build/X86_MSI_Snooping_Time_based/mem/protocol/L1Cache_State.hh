/** \file L1Cache_State.hh
 *
 * Auto generated C++ code started by /gem5/gem5-stable/src/mem/slicc/symbols/Type.py:454
 */

#ifndef __L1Cache_State_HH__
#define __L1Cache_State_HH__

#include <iostream>
#include <string>

#include "mem/protocol/AccessPermission.hh"

// Class definition
/** \enum L1Cache_State
 *  \brief Cache states
 */
enum L1Cache_State {
    L1Cache_State_FIRST,
    L1Cache_State_NP = L1Cache_State_FIRST, /**< Not present in either cache */
    L1Cache_State_I, /**< a L1 cache entry Idle */
    L1Cache_State_S, /**< a L1 cache entry Shared */
    L1Cache_State_M, /**< a L1 cache entry Modified */
    L1Cache_State_IS_AD, /**< L1 idle, issued a load, have not seen response yet */
    L1Cache_State_IS_D, /**< L1 idle, issued a load, saw own GETS, not seen data response yet */
    L1Cache_State_IS_DI, /**< L1 idle, issued a load, saw own GETS, then saw remote GETM not seen data response yet */
    L1Cache_State_S_TI, /**< L1 idle, issued a load, saw own GETS, then saw remote GETM not seen data response yet */
    L1Cache_State_S_TM, /**< L1 idle, issued a load, saw own GETS, then saw remote GETM not seen data response yet */
    L1Cache_State_S_MA, /**< L1 idle, issued a load, saw own GETS, then saw remote GETM not seen data response yet */
    L1Cache_State_IM_AD, /**< L1 idle, issued GETX, have not seen response yet */
    L1Cache_State_IM_D, /**< L1 idle, issued GETX, saw own GETX, not seen data response yet */
    L1Cache_State_IM_DI, /**< lisnfd */
    L1Cache_State_M_TI, /**< L1 idle, issued a load, saw own GETS, then saw remote GETM not seen data response yet */
    L1Cache_State_SI_A, /**< L1 replacing, waiting for ACK. Serive remote load and store misses */
    L1Cache_State_SI, /**< L1 replacing, waiting for ACK. Serive remote load and store misses */
    L1Cache_State_MI_A, /**< L1 invalidating due to other GETM/GETS, waiting for ACK. Serice remote load and store misses */
    L1Cache_State_MI_R, /**< L1 replacing due to replacement, waiting for ACK. Serive remote load and store misses */
    L1Cache_State_NUM
};

// Code to convert from a string to the enumeration
L1Cache_State string_to_L1Cache_State(const std::string& str);

// Code to convert state to a string
std::string L1Cache_State_to_string(const L1Cache_State& obj);

// Code to increment an enumeration type
L1Cache_State &operator++(L1Cache_State &e);

// Code to convert the current state to an access permission
AccessPermission L1Cache_State_to_permission(const L1Cache_State& obj);

std::ostream& operator<<(std::ostream& out, const L1Cache_State& obj);

#endif // __L1Cache_State_HH__
