/** \file Directory_Event.hh
 *
 * Auto generated C++ code started by /gem5/gem5-stable/src/mem/slicc/symbols/Type.py:454
 */

#ifndef __Directory_Event_HH__
#define __Directory_Event_HH__

#include <iostream>
#include <string>


// Class definition
/** \enum Directory_Event
 *  \brief Directory events
 */
enum Directory_Event {
    Directory_Event_FIRST,
    Directory_Event_Fetch = Directory_Event_FIRST, /**< A memory fetch arrives */
    Directory_Event_Data, /**< writeback data arrives */
    Directory_Event_Memory_Data, /**< Fetched data from memory arrives */
    Directory_Event_Memory_Ack, /**< Writeback Ack from memory arrives */
    Directory_Event_DMA_READ, /**< A DMA Read memory request */
    Directory_Event_DMA_WRITE, /**< A DMA Write memory request */
    Directory_Event_GETS, /**< A GETS request */
    Directory_Event_PUTM, /**< A putm arrives */
    Directory_Event_GETM, /**< A GETM request */
    Directory_Event_GETMO, /**< A GETM request from the Owner. Possibly an upgrade was previously seen */
    Directory_Event_UPG, /**< A UPG request */
    Directory_Event_GETSD, /**< A GETS request */
    Directory_Event_PUTMD, /**< A putm arrives */
    Directory_Event_GETMD, /**< A GETM request */
    Directory_Event_GETMOD, /**< A GETM request from the Owner. Possibly an upgrade was previously seen */
    Directory_Event_UPGD, /**< A UPG request */
    Directory_Event_InvLatest, /**< the last sharer has invalidated */
    Directory_Event_GETM_C, /**< getm critical */
    Directory_Event_GETM_NC, /**< getm non critical */
    Directory_Event_GETS_C, /**< gets critical */
    Directory_Event_GETS_NC, /**< gets noncritical */
    Directory_Event_SENDDATA, /**< send data coherence msg received */
    Directory_Event_NoData, /**< send data coherence msg received */
    Directory_Event_WaitForData, /**< send data coherence msg received */
    Directory_Event_DATAACK, /**< ack */
    Directory_Event_NUM
};

// Code to convert from a string to the enumeration
Directory_Event string_to_Directory_Event(const std::string& str);

// Code to convert state to a string
std::string Directory_Event_to_string(const Directory_Event& obj);

// Code to increment an enumeration type
Directory_Event &operator++(Directory_Event &e);
std::ostream& operator<<(std::ostream& out, const Directory_Event& obj);

#endif // __Directory_Event_HH__
