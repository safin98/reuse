/** \file L1Cache_Controller.hh
 *
 * Auto generated C++ code started by /gem5/gem5-stable/src/mem/slicc/symbols/StateMachine.py:257
 * Created by slicc definition of Module "MSI Snooping L1 Cache CMP"
 */

#ifndef __L1Cache_CONTROLLER_HH__
#define __L1Cache_CONTROLLER_HH__

#include <iostream>
#include <sstream>
#include <string>

#include "mem/protocol/TransitionResult.hh"
#include "mem/protocol/Types.hh"
#include "mem/ruby/common/Consumer.hh"
#include "mem/ruby/common/Global.hh"
#include "mem/ruby/slicc_interface/AbstractController.hh"
#include "params/L1Cache_Controller.hh"
#include "mem/protocol/TBETable.hh"
#include "mem/protocol/MessageBuffer.hh"
#include "mem/protocol/TimerTable.hh"
extern std::stringstream L1Cache_transitionComment;

class L1Cache_Controller : public AbstractController
{
  public:
    typedef L1Cache_ControllerParams Params;
    L1Cache_Controller(const Params *p);
    static int getNumControllers();
    void init();

    MessageBuffer* getMandatoryQueue() const;

    void print(std::ostream& out) const;
    void wakeup();
    void resetStats();
    void regStats();
    void collateStats();
    void setNetQueue(const std::string& name, MessageBuffer *b);

    void recordCacheTrace(int cntrl, CacheRecorder* tr);
    Sequencer* getSequencer() const;

    int functionalWriteBuffers(PacketPtr&);

    void countTransition(L1Cache_State state, L1Cache_Event event);
    void possibleTransition(L1Cache_State state, L1Cache_Event event);
    uint64 getEventCount(L1Cache_Event event);
    bool isPossible(L1Cache_State state, L1Cache_Event event);
    uint64 getTransitionCount(L1Cache_State state, L1Cache_Event event);

private:
    Sequencer* m_sequencer_ptr;
    CacheMemory* m_Icache_ptr;
    CacheMemory* m_Dcache_ptr;
    Cycles m_l1_request_latency;
    Cycles m_l1_response_latency;
    bool m_send_evictions;
    int m_is_blocked;
    Cycles m_max_wcet_latency;
    Cycles m_max_arbit_latency;
    Cycles m_max_interC_latency;
    Cycles m_max_intraC_latency;
    Cycles m_max_S_to_M_latency;
    Cycles m_total_latency;
    Cycles m_timeout_latency_CC;
    Cycles m_timeout_latency_CN;
    Cycles m_timeout_latency_NC;
    Cycles m_timeout_latency_NN;
    Cycles m_timeout_0;
    MessageBuffer* m_requestFromCache_ptr;
    MessageBuffer* m_requestFromCacheWB_ptr;
    MessageBuffer* m_responseFromCache_ptr;
    MessageBuffer* m_atomicRequestFromCache_ptr;
    MessageBuffer* m_responseToCache_ptr;
    MessageBuffer* m_requestToCache_ptr;
    MessageBuffer* m_requestToCacheWB_ptr;
    MessageBuffer* m_atomicRequestToCache_ptr;
    TransitionResult doTransition(L1Cache_Event event,
                                  L1Cache_Entry* m_cache_entry_ptr,
                                  L1Cache_TBE* m_tbe_ptr,
                                  const Address addr);

    TransitionResult doTransitionWorker(L1Cache_Event event,
                                        L1Cache_State state,
                                        L1Cache_State& next_state,
                                        L1Cache_TBE*& m_tbe_ptr,
                                        L1Cache_Entry*& m_cache_entry_ptr,
                                        const Address& addr);

    int m_counters[L1Cache_State_NUM][L1Cache_Event_NUM];
    int m_event_counters[L1Cache_Event_NUM];
    bool m_possible[L1Cache_State_NUM][L1Cache_Event_NUM];

    static std::vector<Stats::Vector *> eventVec;
    static std::vector<std::vector<Stats::Vector *> > transVec;
    static int m_num_controllers;
    // Internal functions
    L1Cache_Entry* getL1CacheEntry(const Address& param_addr);
    bool isAtomic(const Address& param_addr);
    L1Cache_Entry* getDCacheEntry(const Address& param_addr);
    L1Cache_Entry* getICacheEntry(const Address& param_addr);
    L1Cache_State getState(L1Cache_TBE* param_tbe, L1Cache_Entry* param_cache_entry, const Address& param_addr);
    Cycles setTimeoutLatency(const bool& param_current, const bool& param_requestor);
    bool is_dataSendState(L1Cache_TBE* param_tbe, const Address& param_addr, L1Cache_Entry* param_cache_entry);
    bool is_dataInvState(L1Cache_TBE* param_tbe, const Address& param_addr, L1Cache_Entry* param_cache_entry);
    bool is_noncriticalotherGETM(L1Cache_TBE* param_tbe, const Address& param_addr, L1Cache_Entry* param_cache_entry);
    bool is_noncriticalotherGETS(L1Cache_TBE* param_tbe, const Address& param_addr, L1Cache_Entry* param_cache_entry);
    bool is_criticalotherGETM(L1Cache_TBE* param_tbe, const Address& param_addr, L1Cache_Entry* param_cache_entry);
    bool is_criticalotherGETS(L1Cache_TBE* param_tbe, const Address& param_addr, L1Cache_Entry* param_cache_entry);
    bool is_dataWaitingState(L1Cache_TBE* param_tbe, const Address& param_addr, L1Cache_Entry* param_cache_entry);
    bool is_atomicInvariant(L1Cache_TBE* param_tbe, const Address& param_addr, L1Cache_Entry* param_cache_entry);
    void setState(L1Cache_TBE* param_tbe, L1Cache_Entry* param_cache_entry, const Address& param_addr, const L1Cache_State& param_state);
    AccessPermission getAccessPermission(const Address& param_addr);
    void functionalRead(const Address& param_addr, Packet* param_pkt);
    int functionalWrite(const Address& param_addr, Packet* param_pkt);
    void setAccessPermission(L1Cache_Entry* param_cache_entry, const Address& param_addr, const L1Cache_State& param_state);
    L1Cache_Event mandatory_request_type_to_event(const RubyRequestType& param_type);
    int getPendingAcks(L1Cache_TBE* param_tbe);

    // Set and Reset for cache_entry variable
    void set_cache_entry(L1Cache_Entry*& m_cache_entry_ptr, AbstractCacheEntry* m_new_cache_entry);
    void unset_cache_entry(L1Cache_Entry*& m_cache_entry_ptr);

    //cache_approx
    // Set and Reset for svc_entry variable
    void set_svc_entry(L1Cache_Entry*& m_cache_entry_ptr, AbstractCacheEntry* m_new_cache_entry);
    void unset_svc_entry(L1Cache_Entry*& m_cache_entry_ptr);


    // Set and Reset for tbe variable
    void set_tbe(L1Cache_TBE*& m_tbe_ptr, L1Cache_TBE* m_new_tbe);
    void unset_tbe(L1Cache_TBE*& m_tbe_ptr);

    // Actions
    /** \brief Issue GETS */
    void as_issueGETSMem(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief Issue GETINSTR */
    void ai_issueGETINSTR(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief Issue GETM */
    void bm_issueGETM(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief Issue inv when latest */
    void issueInv(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief f */
    void setownfinaldest(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief f */
    void setDestmir(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief adding the destination before reissuing */
    void addDest(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief adding the destination before reissuing */
    void addReqDest(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief Issue PUTM */
    void bpm_issuePUTM(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief Issue SENDDATA */
    void bpm_issuesendData(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief Send data from cache to directory */
    void cc_sendDataCacheToDir(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief Send data from cache to cache */
    void cc_sendDataCacheToCache(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief sends eviction information to the processor */
    void forward_eviction_to_cpu(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief new data from mem, notify sequencer the load completed. */
    void r_load_hit(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief data already present, notify sequencer the load completed. */
    void rx_load_hit(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief If not prefetch, notify sequencer that store completed. */
    void s_store_hit(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief If not prefetch, notify sequencer that store completed. */
    void sx_store_hit(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief Allocate TBE (isPrefetch=0, number of invalidates=0) */
    void i_allocateTBE(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief Pop mandatory queue. */
    void k_popMandatoryQueue(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief Calculate latency profile of request */
    void calcLatencyProfile(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief Set start time of request */
    void setStartTime(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief Set start time of request */
    void setTime_SM(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief Set start time of request cache entry */
    void setStartTimeCE(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief Copy start time from CE to TBE */
    void copyStartTimeCETBE(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief Set scheduled time */
    void setFirstScheduledTime(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief Set scheduled time */
    void setFirstScheduledTimeCE(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief Pop incoming request queue and profile the delay within this virtual network */
    void l_popRequestQueue(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief Pop incoming request queue and profile the delay within this virtual network */
    void l_popRequestWBQueue(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief Pop Incoming Response queue and profile the delay within this virtual network */
    void o_popIncomingResponseQueue(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief Deallocate TBE */
    void s_deallocateTBE(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief Write data to cache */
    void u_writeDataToL1Cache(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief unset cache entry */
    void unset_cache_entry(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief Deallocate L1 cache block.  Sets the cache to not present, allowing a replacement in parallel with a fetch. */
    void ff_deallocateL1CacheBlock(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief Set L1 D-cache tag equal to tag of block B. */
    void oo_allocateL1DCacheBlock(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief Set L1 I-cache tag equal to tag of block B. */
    void pp_allocateL1ICacheBlock(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief recycle L1 request queue */
    void z_stallAndWaitMandatoryQueue(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief recycle L1 response queue */
    void z_stallAndWaitRequestQueue(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief recycle l1 request WB queue */
    void z_stallAndWaitRequestWBQueue(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief wake-up dependents */
    void kd_wakeUpDependents(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief wake-up all dependents */
    void ka_wakeUpAllDependents(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief Profile the demand miss */
    void uu_profileInstMiss(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief Profile the demand hit */
    void uu_profileInstHit(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief Profile the demand miss */
    void uu_profileDataMiss(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief Profile the demand hit */
    void uu_profileDataHit(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief Profile the demand hit */
    void uu_profileLoadHit(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief Profile the demand hit */
    void uu_profileStoreHit(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief Profile the demand hit */
    void uu_profileLoadMiss(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief Profile the demand hit */
    void uu_profileStoreMiss(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief Profile the upgrade miss : Store on S */
    void uu_profileUpgradeMiss(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief Profile the demand hit */
    void profile_inv_S(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief Profile the demand hit */
    void profile_inv_S_repl(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief Profile the demand hit */
    void profile_inv_M(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief unset timer */
    void unsetcriticalTimer(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief unset timer */
    void unsetnoncriticalTimer(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief unset 2 timers */
    void unsetTimers(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief start 2 timers */
    void setTimers(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief restart 2 timers */
    void restartTimers(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief restart 2 timers */
    void restartcriticalTimer(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief restart 2 timers */
    void restartnoncriticalTimer(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);
    /** \brief clearing the destination before reissuing */
    void clearDest(L1Cache_TBE*& m_tbe_ptr, L1Cache_Entry*& m_cache_entry_ptr, const Address& addr);

    // Objects
    TBETable<L1Cache_TBE>* m_TBEs_ptr;
    MessageBuffer* m_mandatoryQueue_ptr;
    TimerTable* m_TimerTable_CC_ptr;
    TimerTable* m_TimerTable_CN_ptr;
    TimerTable* m_TimerTable_NC_ptr;
    TimerTable* m_TimerTable_NN_ptr;
};
#endif // __L1Cache_CONTROLLER_H__
