/** \file L1Cache_Event.hh
 *
 * Auto generated C++ code started by /gem5/gem5-stable/src/mem/slicc/symbols/Type.py:454
 */

#ifndef __L1Cache_Event_HH__
#define __L1Cache_Event_HH__

#include <iostream>
#include <string>


// Class definition
/** \enum L1Cache_Event
 *  \brief Cache events
 */
enum L1Cache_Event {
    L1Cache_Event_FIRST,
    L1Cache_Event_Load = L1Cache_Event_FIRST, /**< Load request from processor */
    L1Cache_Event_Store, /**< Store request from processor */
    L1Cache_Event_Ifetch, /**< Instruction fetch request from processor */
    L1Cache_Event_RMW_Read, /**< Read-modify-write read from processor */
    L1Cache_Event_RMW_Write, /**< Read-modify-write write from processor */
    L1Cache_Event_Replacement, /**< Replacement from processor */
    L1Cache_Event_Other_GETS_C, /**< Remote processor doing load */
    L1Cache_Event_Other_GETS_NC, /**< Remote processor doing load */
    L1Cache_Event_Other_GETM_C, /**< Remote processor doing store */
    L1Cache_Event_Other_GETM_NC, /**< Remote processor doing store */
    L1Cache_Event_Other_GETI_C, /**< Remote processor doing instruction fetch */
    L1Cache_Event_Other_GETI_NC, /**< Remote processor doing instruction fetch */
    L1Cache_Event_Other_PUTM, /**< Remote processor doing a PUTM */
    L1Cache_Event_OWN_GETS, /**< Processor sees its own GETS coherence message ordered on the network */
    L1Cache_Event_OWN_GETM, /**< Processor sees its own GETM coherence message ordered on the network */
    L1Cache_Event_OWN_GETI, /**< Procssor sees its own GETI coherence message ordered on the network */
    L1Cache_Event_OWN_PUTM, /**< Processor sees its own PUTM */
    L1Cache_Event_INV, /**< Processor sees INV signal from Mem */
    L1Cache_Event_Data, /**< Data sent by shared mem. No cache-to-cache transfer */
    L1Cache_Event_Ack, /**< Ack for processor */
    L1Cache_Event_Timeout_noinv, /**< timeout for the cacheline */
    L1Cache_Event_Timeout_noinv_noncric, /**< timeout for the cacheline */
    L1Cache_Event_Timeout_CC, /**< timeout CC for the cacheline */
    L1Cache_Event_Timeout_CN, /**< timeout CN for the cacheline */
    L1Cache_Event_Timeout_NC, /**< timeout NC for the cacheline */
    L1Cache_Event_Timeout_NN, /**< timeout NN for the cacheline */
    L1Cache_Event_Own_SENDDATA, /**< own SENDDATA */
    L1Cache_Event_Other_SENDDATA, /**< other SENDDATA */
    L1Cache_Event_Own_SELFINV, /**< own SENDDATA */
    L1Cache_Event_Other_SELFINV, /**< other SENDDATA */
    L1Cache_Event_Own_SELFINV_SPL, /**< own SENDDATA */
    L1Cache_Event_ALLINV, /**< ALLINV */
    L1Cache_Event_NUM
};

// Code to convert from a string to the enumeration
L1Cache_Event string_to_L1Cache_Event(const std::string& str);

// Code to convert state to a string
std::string L1Cache_Event_to_string(const L1Cache_Event& obj);

// Code to increment an enumeration type
L1Cache_Event &operator++(L1Cache_Event &e);
std::ostream& operator<<(std::ostream& out, const L1Cache_Event& obj);

#endif // __L1Cache_Event_HH__
