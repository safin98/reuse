// Auto generated C++ code started by /gem5/gem5-stable/src/mem/slicc/symbols/StateMachine.py:1065
// L1Cache: MSI Snooping L1 Cache CMP

#include <sys/types.h>
#include <unistd.h>

#include <cassert>

#include "base/misc.hh"
#include "debug/RubySlicc.hh"
#include "mem/protocol/L1Cache_Controller.hh"
#include "mem/protocol/L1Cache_Event.hh"
#include "mem/protocol/L1Cache_State.hh"
#include "mem/protocol/Types.hh"
#include "mem/ruby/common/Global.hh"
#include "mem/ruby/system/System.hh"
#include "mem/ruby/slicc_interface/RubySlicc_includes.hh"

using namespace std;

void
L1Cache_Controller::wakeup()
{
    int counter = 0;
    while (true) {
        // Some cases will put us into an infinite loop without this limit
   //niv
      //  assert(counter <= m_transitions_per_cycle);
        if (counter == m_transitions_per_cycle) {
            // Count how often we are fully utilized
            m_fully_busy_cycles++;

            // Wakeup in another cycle and try again
      //      scheduleEvent(Cycles(1));
      //      break;
        }
            // L1CacheInPort responseNetwork_in
            m_cur_in_port = 1;
                        if ((((*m_responseToCache_ptr)).isReady())) {
                            {
                                // Declare message
                                const ResponseMsg* in_msg_ptr M5_VAR_USED;
                                in_msg_ptr = dynamic_cast<const ResponseMsg *>(((*m_responseToCache_ptr)).peek());
                                assert(in_msg_ptr != NULL); // Check the cast result
                                if (m_is_blocking &&
                                    (m_block_map.count(in_msg_ptr->m_Addr) == 1) &&
                                    (m_block_map[in_msg_ptr->m_Addr] == &(*m_responseToCache_ptr))) {
                                        (*m_responseToCache_ptr).delayHead();
                                        DPRINTF(RubySlicc, "Delaying in_msg_ptr %s\n", *in_msg_ptr);
                                        continue;
                                }
                                            
                            DPRINTF(RubySlicc, "MSI_Snooping_Time_based_base-cache.sm:469: Response address:%s \n", ((*in_msg_ptr)).m_Addr);
                            L1Cache_Entry* cache_entry
                             = (getL1CacheEntry(((*in_msg_ptr)).m_Addr));
                            L1Cache_TBE* tbe
                             = (((*m_TBEs_ptr)).lookup(((*in_msg_ptr)).m_Addr));
                                if ((((*in_msg_ptr)).m_Type == CoherenceResponseType_DATA)) {
                                    DPRINTF(RubySlicc, "MSI_Snooping_Time_based_base-cache.sm:478: DATA SENT TYPE FROM DIR %s\n ", ((*in_msg_ptr)).m_Addr);
                                    {

                                        TransitionResult result = doTransition(L1Cache_Event_Data, cache_entry, tbe, ((*in_msg_ptr)).m_Addr); 

                                        if (result == TransitionResult_Valid) {
                                            counter++;
                                            continue; // Check the first port again
                                        }

                                        if (result == TransitionResult_ResourceStall) {
                                            scheduleEvent(Cycles(1));

                                            // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                        }
                                    }
                                    ;
                                }
                                    if ((((*in_msg_ptr)).m_Type == CoherenceResponseType_DATA_TO_CACHE)) {
                                        {

                                            TransitionResult result = doTransition(L1Cache_Event_Data, cache_entry, tbe, ((*in_msg_ptr)).m_Addr); 

                                            if (result == TransitionResult_Valid) {
                                                counter++;
                                                continue; // Check the first port again
                                            }

                                            if (result == TransitionResult_ResourceStall) {
                                                scheduleEvent(Cycles(1));

                                                // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                            }
                                        }
                                        ;
                                    } else {
                                            if ((((*in_msg_ptr)).m_Type == CoherenceResponseType_MEMORY_ACK)) {
                                                {

                                                    TransitionResult result = doTransition(L1Cache_Event_Ack, cache_entry, tbe, ((*in_msg_ptr)).m_Addr); 

                                                    if (result == TransitionResult_Valid) {
                                                        counter++;
                                                        continue; // Check the first port again
                                                    }

                                                    if (result == TransitionResult_ResourceStall) {
                                                        scheduleEvent(Cycles(1));

                                                        // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                                    }
                                                }
                                                ;
                                            }
                                        }
                                        }
                                    }
            // L1CacheInPort requestNetwork_in
            m_cur_in_port = 0;
                        if ((((*m_requestToCache_ptr)).isReady())) {
                            {
                                // Declare message
                                const RequestMsg* in_msg_ptr M5_VAR_USED;
                                in_msg_ptr = dynamic_cast<const RequestMsg *>(((*m_requestToCache_ptr)).peek());
                                assert(in_msg_ptr != NULL); // Check the cast result
                                if (m_is_blocking &&
                                    (m_block_map.count(in_msg_ptr->m_Addr) == 1) &&
                                    (m_block_map[in_msg_ptr->m_Addr] == &(*m_requestToCache_ptr))) {
                                        (*m_requestToCache_ptr).delayHead();
                                        DPRINTF(RubySlicc, "Delaying in_msg_ptr %s\n", *in_msg_ptr);
                                        continue;
                                }
                                            
                            L1Cache_Entry* cache_entry
                             = (getL1CacheEntry(((*in_msg_ptr)).m_Addr));
                            L1Cache_TBE* tbe
                             = (((*m_TBEs_ptr)).lookup(((*in_msg_ptr)).m_Addr));
                            int numproc_c
                             = (getnumproc());
                            #ifndef NDEBUG
                            if (!(((((*in_msg_ptr)).m_Destination).isElement(m_machineID)))) {
                                panic("Runtime Error at MSI_Snooping_Time_based_base-cache.sm:505: %s.\n", "assert failure");

                            }
                            #endif
                            ;
                                if ((((*in_msg_ptr)).m_Type == CoherenceRequestType_GETS)) {
                                        if ((((*in_msg_ptr)).m_Requestor == m_machineID)) {
                                            {

                                                TransitionResult result = doTransition(L1Cache_Event_OWN_GETS, cache_entry, tbe, ((*in_msg_ptr)).m_Addr); 

                                                if (result == TransitionResult_Valid) {
                                                    counter++;
                                                    continue; // Check the first port again
                                                }

                                                if (result == TransitionResult_ResourceStall) {
                                                    scheduleEvent(Cycles(1));

                                                    // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                                }
                                            }
                                            ;
                                        } else {
                                                if ((((IDToInt(((m_machineID).getNum()))) >= numproc_c) && (is_criticalotherGETS(tbe, ((*in_msg_ptr)).m_Addr, cache_entry)))) {
                                                        if ((((IDToInt(((((*in_msg_ptr)).m_Requestor).getNum()))) >= numproc_c) && ((*cache_entry).m_Timeout == m_timeout_0))) {
                                                            (*cache_entry).m_Timeout = (setTimeoutLatency((false), (false)));
                                                        } else {
                                                                if ((((IDToInt(((((*in_msg_ptr)).m_Requestor).getNum()))) < numproc_c) && (is_noncriticalotherGETS(tbe, ((*in_msg_ptr)).m_Addr, cache_entry)))) {
                                                                    (*cache_entry).m_Timeout = (setTimeoutLatency((false), (true)));
                                                                }
                                                            }
                                                        } else {
                                                                if ((((IDToInt(((m_machineID).getNum()))) < numproc_c) && (is_criticalotherGETS(tbe, ((*in_msg_ptr)).m_Addr, cache_entry)))) {
                                                                        if ((((IDToInt(((((*in_msg_ptr)).m_Requestor).getNum()))) >= numproc_c) && ((*cache_entry).m_Timeout == m_timeout_0))) {
                                                                            (*cache_entry).m_Timeout = (setTimeoutLatency((true), (false)));
                                                                        } else {
                                                                                if (((IDToInt(((((*in_msg_ptr)).m_Requestor).getNum()))) < numproc_c)) {
                                                                                    (*cache_entry).m_Timeout = (setTimeoutLatency((true), (true)));
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                        if ((cache_entry != NULL)) {
                                                                        }
                                                                            if ((cache_entry != NULL)) {
                                                                            }
                                                                                if ((tbe != NULL)) {
                                                                                }
                                                                                    if (((isAtomic(((*in_msg_ptr)).m_Addr)) && (is_dataWaitingState(tbe, ((*in_msg_ptr)).m_Addr, cache_entry)))) {
                                                                                    } else {
                                                                                        L1Cache_State returnState
                                                                                         = (getState(tbe, cache_entry, ((*in_msg_ptr)).m_Addr));
                                                                                            if (((((returnState == L1Cache_State_MI_A) && (((*cache_entry).m_finalDestinationSet_C).isEmpty())) && ((IDToInt(((((*in_msg_ptr)).m_Requestor).getNum()))) < numproc_c)) && ((IDToInt(((m_machineID).getNum()))) < numproc_c))) {
                                                                                                    if ((is_dataSendState(tbe, ((*in_msg_ptr)).m_Addr, cache_entry))) {
                                                                                                            if (((IDToInt(((((*in_msg_ptr)).m_Requestor).getNum()))) >= numproc_c)) {
                                                                                                                    if ((((*cache_entry).m_finalDestinationSet_NC).isEmpty())) {
                                                                                                                        (((*cache_entry).m_finalDestinationSet_NC).add(((*in_msg_ptr)).m_Requestor));
                                                                                                                        (((*cache_entry).m_finalDestinationSet_NC).add((map_Address_to_Directory(((*in_msg_ptr)).m_Addr))));
                                                                                                                    }
                                                                                                                } else {
                                                                                                                        if (((IDToInt(((((*in_msg_ptr)).m_Requestor).getNum()))) < numproc_c)) {
                                                                                                                                if ((((*cache_entry).m_finalDestinationSet_C).isEmpty())) {
                                                                                                                                    (((*cache_entry).m_finalDestinationSet_C).add(((*in_msg_ptr)).m_Requestor));
                                                                                                                                    (((*cache_entry).m_finalDestinationSet_C).add((map_Address_to_Directory(((*in_msg_ptr)).m_Addr))));
                                                                                                                                }
                                                                                                                            }
                                                                                                                        }
                                                                                                                    }
                                                                                                                    {

                                                                                                                        TransitionResult result = doTransition(L1Cache_Event_Other_GETS_C, cache_entry, tbe, ((*in_msg_ptr)).m_Addr); 

                                                                                                                        if (result == TransitionResult_Valid) {
                                                                                                                            counter++;
                                                                                                                            continue; // Check the first port again
                                                                                                                        }

                                                                                                                        if (result == TransitionResult_ResourceStall) {
                                                                                                                            scheduleEvent(Cycles(1));

                                                                                                                            // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                                                                                                        }
                                                                                                                    }
                                                                                                                    ;
                                                                                                                } else {
                                                                                                                        if (((((returnState == L1Cache_State_MI_A) && ((((*cache_entry).m_finalDestinationSet_C).isEmpty()) == (false))) && ((IDToInt(((((*in_msg_ptr)).m_Requestor).getNum()))) < numproc_c)) && ((IDToInt(((m_machineID).getNum()))) >= numproc_c))) {
                                                                                                                                if ((is_dataSendState(tbe, ((*in_msg_ptr)).m_Addr, cache_entry))) {
                                                                                                                                        if (((IDToInt(((((*in_msg_ptr)).m_Requestor).getNum()))) >= numproc_c)) {
                                                                                                                                                if ((((*cache_entry).m_finalDestinationSet_NC).isEmpty())) {
                                                                                                                                                    (((*cache_entry).m_finalDestinationSet_NC).add(((*in_msg_ptr)).m_Requestor));
                                                                                                                                                    (((*cache_entry).m_finalDestinationSet_NC).add((map_Address_to_Directory(((*in_msg_ptr)).m_Addr))));
                                                                                                                                                }
                                                                                                                                            } else {
                                                                                                                                                    if (((IDToInt(((((*in_msg_ptr)).m_Requestor).getNum()))) < numproc_c)) {
                                                                                                                                                            if ((((*cache_entry).m_finalDestinationSet_C).isEmpty())) {
                                                                                                                                                                (((*cache_entry).m_finalDestinationSet_C).add(((*in_msg_ptr)).m_Requestor));
                                                                                                                                                                (((*cache_entry).m_finalDestinationSet_C).add((map_Address_to_Directory(((*in_msg_ptr)).m_Addr))));
                                                                                                                                                            }
                                                                                                                                                        }
                                                                                                                                                    }
                                                                                                                                                }
                                                                                                                                                {

                                                                                                                                                    TransitionResult result = doTransition(L1Cache_Event_Other_GETS_NC, cache_entry, tbe, ((*in_msg_ptr)).m_Addr); 

                                                                                                                                                    if (result == TransitionResult_Valid) {
                                                                                                                                                        counter++;
                                                                                                                                                        continue; // Check the first port again
                                                                                                                                                    }

                                                                                                                                                    if (result == TransitionResult_ResourceStall) {
                                                                                                                                                        scheduleEvent(Cycles(1));

                                                                                                                                                        // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                                                                                                                                    }
                                                                                                                                                }
                                                                                                                                                ;
                                                                                                                                            } else {
                                                                                                                                                    if (((returnState == L1Cache_State_M) || (returnState == L1Cache_State_M_TI))) {
                                                                                                                                                            if ((is_dataSendState(tbe, ((*in_msg_ptr)).m_Addr, cache_entry))) {
                                                                                                                                                                    if (((IDToInt(((((*in_msg_ptr)).m_Requestor).getNum()))) >= numproc_c)) {
                                                                                                                                                                            if ((((*cache_entry).m_finalDestinationSet_NC).isEmpty())) {
                                                                                                                                                                                (((*cache_entry).m_finalDestinationSet_NC).add(((*in_msg_ptr)).m_Requestor));
                                                                                                                                                                                (((*cache_entry).m_finalDestinationSet_NC).add((map_Address_to_Directory(((*in_msg_ptr)).m_Addr))));
                                                                                                                                                                            }
                                                                                                                                                                        } else {
                                                                                                                                                                                if (((IDToInt(((((*in_msg_ptr)).m_Requestor).getNum()))) < numproc_c)) {
                                                                                                                                                                                        if ((((*cache_entry).m_finalDestinationSet_C).isEmpty())) {
                                                                                                                                                                                            (((*cache_entry).m_finalDestinationSet_C).add(((*in_msg_ptr)).m_Requestor));
                                                                                                                                                                                            (((*cache_entry).m_finalDestinationSet_C).add((map_Address_to_Directory(((*in_msg_ptr)).m_Addr))));
                                                                                                                                                                                        }
                                                                                                                                                                                    }
                                                                                                                                                                                }
                                                                                                                                                                            }
                                                                                                                                                                                if (((IDToInt(((((*in_msg_ptr)).m_Requestor).getNum()))) < numproc_c)) {
                                                                                                                                                                                    {

                                                                                                                                                                                        TransitionResult result = doTransition(L1Cache_Event_Other_GETS_C, cache_entry, tbe, ((*in_msg_ptr)).m_Addr); 

                                                                                                                                                                                        if (result == TransitionResult_Valid) {
                                                                                                                                                                                            counter++;
                                                                                                                                                                                            continue; // Check the first port again
                                                                                                                                                                                        }

                                                                                                                                                                                        if (result == TransitionResult_ResourceStall) {
                                                                                                                                                                                            scheduleEvent(Cycles(1));

                                                                                                                                                                                            // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                                                                                                                                                                        }
                                                                                                                                                                                    }
                                                                                                                                                                                    ;
                                                                                                                                                                                } else {
                                                                                                                                                                                    {

                                                                                                                                                                                        TransitionResult result = doTransition(L1Cache_Event_Other_GETS_NC, cache_entry, tbe, ((*in_msg_ptr)).m_Addr); 

                                                                                                                                                                                        if (result == TransitionResult_Valid) {
                                                                                                                                                                                            counter++;
                                                                                                                                                                                            continue; // Check the first port again
                                                                                                                                                                                        }

                                                                                                                                                                                        if (result == TransitionResult_ResourceStall) {
                                                                                                                                                                                            scheduleEvent(Cycles(1));

                                                                                                                                                                                            // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                                                                                                                                                                        }
                                                                                                                                                                                    }
                                                                                                                                                                                    ;
                                                                                                                                                                                }
                                                                                                                                                                            } else {
                                                                                                                                                                                    if ((is_dataSendState(tbe, ((*in_msg_ptr)).m_Addr, cache_entry))) {
                                                                                                                                                                                            if (((IDToInt(((((*in_msg_ptr)).m_Requestor).getNum()))) >= numproc_c)) {
                                                                                                                                                                                                    if ((((*cache_entry).m_finalDestinationSet_NC).isEmpty())) {
                                                                                                                                                                                                        (((*cache_entry).m_finalDestinationSet_NC).add(((*in_msg_ptr)).m_Requestor));
                                                                                                                                                                                                        (((*cache_entry).m_finalDestinationSet_NC).add((map_Address_to_Directory(((*in_msg_ptr)).m_Addr))));
                                                                                                                                                                                                    }
                                                                                                                                                                                                } else {
                                                                                                                                                                                                        if (((IDToInt(((((*in_msg_ptr)).m_Requestor).getNum()))) < numproc_c)) {
                                                                                                                                                                                                                if ((((*cache_entry).m_finalDestinationSet_C).isEmpty())) {
                                                                                                                                                                                                                    (((*cache_entry).m_finalDestinationSet_C).add(((*in_msg_ptr)).m_Requestor));
                                                                                                                                                                                                                    (((*cache_entry).m_finalDestinationSet_C).add((map_Address_to_Directory(((*in_msg_ptr)).m_Addr))));
                                                                                                                                                                                                                }
                                                                                                                                                                                                            }
                                                                                                                                                                                                        }
                                                                                                                                                                                                    }
                                                                                                                                                                                                        if ((((IDToInt(((((*in_msg_ptr)).m_Requestor).getNum()))) < numproc_c) && ((IDToInt(((m_machineID).getNum()))) >= numproc_c))) {
                                                                                                                                                                                                            {

                                                                                                                                                                                                                TransitionResult result = doTransition(L1Cache_Event_Other_GETS_C, cache_entry, tbe, ((*in_msg_ptr)).m_Addr); 

                                                                                                                                                                                                                if (result == TransitionResult_Valid) {
                                                                                                                                                                                                                    counter++;
                                                                                                                                                                                                                    continue; // Check the first port again
                                                                                                                                                                                                                }

                                                                                                                                                                                                                if (result == TransitionResult_ResourceStall) {
                                                                                                                                                                                                                    scheduleEvent(Cycles(1));

                                                                                                                                                                                                                    // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                                                                                                                                                                                                }
                                                                                                                                                                                                            }
                                                                                                                                                                                                            ;
                                                                                                                                                                                                        } else {
                                                                                                                                                                                                            {

                                                                                                                                                                                                                TransitionResult result = doTransition(L1Cache_Event_Other_GETS_NC, cache_entry, tbe, ((*in_msg_ptr)).m_Addr); 

                                                                                                                                                                                                                if (result == TransitionResult_Valid) {
                                                                                                                                                                                                                    counter++;
                                                                                                                                                                                                                    continue; // Check the first port again
                                                                                                                                                                                                                }

                                                                                                                                                                                                                if (result == TransitionResult_ResourceStall) {
                                                                                                                                                                                                                    scheduleEvent(Cycles(1));

                                                                                                                                                                                                                    // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                                                                                                                                                                                                }
                                                                                                                                                                                                            }
                                                                                                                                                                                                            ;
                                                                                                                                                                                                        }
                                                                                                                                                                                                    }
                                                                                                                                                                                                }
                                                                                                                                                                                            }
                                                                                                                                                                                        }
                                                                                                                                                                                    }
                                                                                                                                                                                } else {
                                                                                                                                                                                        if ((((*in_msg_ptr)).m_Type == CoherenceRequestType_INV)) {
                                                                                                                                                                                            {

                                                                                                                                                                                                TransitionResult result = doTransition(L1Cache_Event_INV, cache_entry, tbe, ((*in_msg_ptr)).m_Addr); 

                                                                                                                                                                                                if (result == TransitionResult_Valid) {
                                                                                                                                                                                                    counter++;
                                                                                                                                                                                                    continue; // Check the first port again
                                                                                                                                                                                                }

                                                                                                                                                                                                if (result == TransitionResult_ResourceStall) {
                                                                                                                                                                                                    scheduleEvent(Cycles(1));

                                                                                                                                                                                                    // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                                                                                                                                                                                }
                                                                                                                                                                                            }
                                                                                                                                                                                            ;
                                                                                                                                                                                        } else {
                                                                                                                                                                                                if ((((*in_msg_ptr)).m_Type == CoherenceRequestType_GETI)) {
                                                                                                                                                                                                        if ((((*in_msg_ptr)).m_Requestor == m_machineID)) {
                                                                                                                                                                                                            {

                                                                                                                                                                                                                TransitionResult result = doTransition(L1Cache_Event_OWN_GETI, cache_entry, tbe, ((*in_msg_ptr)).m_Addr); 

                                                                                                                                                                                                                if (result == TransitionResult_Valid) {
                                                                                                                                                                                                                    counter++;
                                                                                                                                                                                                                    continue; // Check the first port again
                                                                                                                                                                                                                }

                                                                                                                                                                                                                if (result == TransitionResult_ResourceStall) {
                                                                                                                                                                                                                    scheduleEvent(Cycles(1));

                                                                                                                                                                                                                    // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                                                                                                                                                                                                }
                                                                                                                                                                                                            }
                                                                                                                                                                                                            ;
                                                                                                                                                                                                        } else {
                                                                                                                                                                                                                if ((((IDToInt(((((*in_msg_ptr)).m_Requestor).getNum()))) < numproc_c) && ((IDToInt(((m_machineID).getNum()))) >= numproc_c))) {
                                                                                                                                                                                                                    {

                                                                                                                                                                                                                        TransitionResult result = doTransition(L1Cache_Event_Other_GETI_C, cache_entry, tbe, ((*in_msg_ptr)).m_Addr); 

                                                                                                                                                                                                                        if (result == TransitionResult_Valid) {
                                                                                                                                                                                                                            counter++;
                                                                                                                                                                                                                            continue; // Check the first port again
                                                                                                                                                                                                                        }

                                                                                                                                                                                                                        if (result == TransitionResult_ResourceStall) {
                                                                                                                                                                                                                            scheduleEvent(Cycles(1));

                                                                                                                                                                                                                            // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                                                                                                                                                                                                        }
                                                                                                                                                                                                                    }
                                                                                                                                                                                                                    ;
                                                                                                                                                                                                                } else {
                                                                                                                                                                                                                    {

                                                                                                                                                                                                                        TransitionResult result = doTransition(L1Cache_Event_Other_GETI_NC, cache_entry, tbe, ((*in_msg_ptr)).m_Addr); 

                                                                                                                                                                                                                        if (result == TransitionResult_Valid) {
                                                                                                                                                                                                                            counter++;
                                                                                                                                                                                                                            continue; // Check the first port again
                                                                                                                                                                                                                        }

                                                                                                                                                                                                                        if (result == TransitionResult_ResourceStall) {
                                                                                                                                                                                                                            scheduleEvent(Cycles(1));

                                                                                                                                                                                                                            // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                                                                                                                                                                                                        }
                                                                                                                                                                                                                    }
                                                                                                                                                                                                                    ;
                                                                                                                                                                                                                }
                                                                                                                                                                                                            }
                                                                                                                                                                                                        } else {
                                                                                                                                                                                                                if ((((*in_msg_ptr)).m_Type == CoherenceRequestType_GETM)) {
                                                                                                                                                                                                                        if ((((*in_msg_ptr)).m_Requestor == m_machineID)) {
                                                                                                                                                                                                                            {

                                                                                                                                                                                                                                TransitionResult result = doTransition(L1Cache_Event_OWN_GETM, cache_entry, tbe, ((*in_msg_ptr)).m_Addr); 

                                                                                                                                                                                                                                if (result == TransitionResult_Valid) {
                                                                                                                                                                                                                                    counter++;
                                                                                                                                                                                                                                    continue; // Check the first port again
                                                                                                                                                                                                                                }

                                                                                                                                                                                                                                if (result == TransitionResult_ResourceStall) {
                                                                                                                                                                                                                                    scheduleEvent(Cycles(1));

                                                                                                                                                                                                                                    // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                                                                                                                                                                                                                }
                                                                                                                                                                                                                            }
                                                                                                                                                                                                                            ;
                                                                                                                                                                                                                        } else {
                                                                                                                                                                                                                                if ((((IDToInt(((m_machineID).getNum()))) >= numproc_c) && (is_criticalotherGETM(tbe, ((*in_msg_ptr)).m_Addr, cache_entry)))) {
                                                                                                                                                                                                                                        if ((((IDToInt(((((*in_msg_ptr)).m_Requestor).getNum()))) >= numproc_c) && ((*cache_entry).m_Timeout == m_timeout_0))) {
                                                                                                                                                                                                                                            (*cache_entry).m_Timeout = (setTimeoutLatency((false), (false)));
                                                                                                                                                                                                                                        } else {
                                                                                                                                                                                                                                                if ((((IDToInt(((((*in_msg_ptr)).m_Requestor).getNum()))) < numproc_c) && (is_noncriticalotherGETM(tbe, ((*in_msg_ptr)).m_Addr, cache_entry)))) {
                                                                                                                                                                                                                                                    (*cache_entry).m_Timeout = (setTimeoutLatency((false), (true)));
                                                                                                                                                                                                                                                }
                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                        } else {
                                                                                                                                                                                                                                                if ((((IDToInt(((m_machineID).getNum()))) < numproc_c) && (is_criticalotherGETM(tbe, ((*in_msg_ptr)).m_Addr, cache_entry)))) {
                                                                                                                                                                                                                                                        if ((((IDToInt(((((*in_msg_ptr)).m_Requestor).getNum()))) >= numproc_c) && ((*cache_entry).m_Timeout == m_timeout_0))) {
                                                                                                                                                                                                                                                            (*cache_entry).m_Timeout = (setTimeoutLatency((true), (false)));
                                                                                                                                                                                                                                                        } else {
                                                                                                                                                                                                                                                                if (((IDToInt(((((*in_msg_ptr)).m_Requestor).getNum()))) < numproc_c)) {
                                                                                                                                                                                                                                                                    (*cache_entry).m_Timeout = (setTimeoutLatency((true), (true)));
                                                                                                                                                                                                                                                                }
                                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                                        }
                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                        if ((cache_entry != NULL)) {
                                                                                                                                                                                                                                                        }
                                                                                                                                                                                                                                                            if ((cache_entry != NULL)) {
                                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                                                if ((tbe != NULL)) {
                                                                                                                                                                                                                                                                }
                                                                                                                                                                                                                                                                    if (((isAtomic(((*in_msg_ptr)).m_Addr)) && (is_dataWaitingState(tbe, ((*in_msg_ptr)).m_Addr, cache_entry)))) {
                                                                                                                                                                                                                                                                    } else {
                                                                                                                                                                                                                                                                        L1Cache_State returnState
                                                                                                                                                                                                                                                                         = (getState(tbe, cache_entry, ((*in_msg_ptr)).m_Addr));
                                                                                                                                                                                                                                                                            if (((((((returnState == L1Cache_State_MI_A) || (returnState == L1Cache_State_SI_A)) || (returnState == L1Cache_State_S_TM)) && (((*cache_entry).m_finalDestinationSet_C).isEmpty())) && ((IDToInt(((((*in_msg_ptr)).m_Requestor).getNum()))) < numproc_c)) && ((IDToInt(((m_machineID).getNum()))) < numproc_c))) {
                                                                                                                                                                                                                                                                                    if (((is_dataSendState(tbe, ((*in_msg_ptr)).m_Addr, cache_entry)) || (is_dataInvState(tbe, ((*in_msg_ptr)).m_Addr, cache_entry)))) {
                                                                                                                                                                                                                                                                                            if (((IDToInt(((((*in_msg_ptr)).m_Requestor).getNum()))) >= numproc_c)) {
                                                                                                                                                                                                                                                                                                    if ((((*cache_entry).m_finalDestinationSet_NC).isEmpty())) {
                                                                                                                                                                                                                                                                                                        (((*cache_entry).m_finalDestinationSet_NC).add(((*in_msg_ptr)).m_Requestor));
                                                                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                                                                } else {
                                                                                                                                                                                                                                                                                                        if (((IDToInt(((((*in_msg_ptr)).m_Requestor).getNum()))) < numproc_c)) {
                                                                                                                                                                                                                                                                                                                if ((((*cache_entry).m_finalDestinationSet_C).isEmpty())) {
                                                                                                                                                                                                                                                                                                                    (((*cache_entry).m_finalDestinationSet_C).add(((*in_msg_ptr)).m_Requestor));
                                                                                                                                                                                                                                                                                                                }
                                                                                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                                                                                        }
                                                                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                                                                    {

                                                                                                                                                                                                                                                                                                        TransitionResult result = doTransition(L1Cache_Event_Other_GETM_C, cache_entry, tbe, ((*in_msg_ptr)).m_Addr); 

                                                                                                                                                                                                                                                                                                        if (result == TransitionResult_Valid) {
                                                                                                                                                                                                                                                                                                            counter++;
                                                                                                                                                                                                                                                                                                            continue; // Check the first port again
                                                                                                                                                                                                                                                                                                        }

                                                                                                                                                                                                                                                                                                        if (result == TransitionResult_ResourceStall) {
                                                                                                                                                                                                                                                                                                            scheduleEvent(Cycles(1));

                                                                                                                                                                                                                                                                                                            // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                                                                                                                                                                                                                                                                                        }
                                                                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                                                                    ;
                                                                                                                                                                                                                                                                                                } else {
                                                                                                                                                                                                                                                                                                        if (((((((returnState == L1Cache_State_MI_A) || (returnState == L1Cache_State_SI_A)) || (returnState == L1Cache_State_S_TM)) && ((((*cache_entry).m_finalDestinationSet_C).isEmpty()) == (false))) && ((IDToInt(((((*in_msg_ptr)).m_Requestor).getNum()))) < numproc_c)) && ((IDToInt(((m_machineID).getNum()))) >= numproc_c))) {
                                                                                                                                                                                                                                                                                                                if (((is_dataSendState(tbe, ((*in_msg_ptr)).m_Addr, cache_entry)) || (is_dataInvState(tbe, ((*in_msg_ptr)).m_Addr, cache_entry)))) {
                                                                                                                                                                                                                                                                                                                        if (((IDToInt(((((*in_msg_ptr)).m_Requestor).getNum()))) >= numproc_c)) {
                                                                                                                                                                                                                                                                                                                                if ((((*cache_entry).m_finalDestinationSet_NC).isEmpty())) {
                                                                                                                                                                                                                                                                                                                                    (((*cache_entry).m_finalDestinationSet_NC).add(((*in_msg_ptr)).m_Requestor));
                                                                                                                                                                                                                                                                                                                                }
                                                                                                                                                                                                                                                                                                                            } else {
                                                                                                                                                                                                                                                                                                                                    if (((IDToInt(((((*in_msg_ptr)).m_Requestor).getNum()))) < numproc_c)) {
                                                                                                                                                                                                                                                                                                                                            if ((((*cache_entry).m_finalDestinationSet_C).isEmpty())) {
                                                                                                                                                                                                                                                                                                                                                (((*cache_entry).m_finalDestinationSet_C).add(((*in_msg_ptr)).m_Requestor));
                                                                                                                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                                                                                                                        }
                                                                                                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                                                                                                }
                                                                                                                                                                                                                                                                                                                                {

                                                                                                                                                                                                                                                                                                                                    TransitionResult result = doTransition(L1Cache_Event_Other_GETM_NC, cache_entry, tbe, ((*in_msg_ptr)).m_Addr); 

                                                                                                                                                                                                                                                                                                                                    if (result == TransitionResult_Valid) {
                                                                                                                                                                                                                                                                                                                                        counter++;
                                                                                                                                                                                                                                                                                                                                        continue; // Check the first port again
                                                                                                                                                                                                                                                                                                                                    }

                                                                                                                                                                                                                                                                                                                                    if (result == TransitionResult_ResourceStall) {
                                                                                                                                                                                                                                                                                                                                        scheduleEvent(Cycles(1));

                                                                                                                                                                                                                                                                                                                                        // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                                                                                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                                                                                                }
                                                                                                                                                                                                                                                                                                                                ;
                                                                                                                                                                                                                                                                                                                            } else {
                                                                                                                                                                                                                                                                                                                                    if (((((returnState == L1Cache_State_M) || (returnState == L1Cache_State_S)) || (returnState == L1Cache_State_S_TI)) || (returnState == L1Cache_State_M_TI))) {
                                                                                                                                                                                                                                                                                                                                            if (((is_dataSendState(tbe, ((*in_msg_ptr)).m_Addr, cache_entry)) || (is_dataInvState(tbe, ((*in_msg_ptr)).m_Addr, cache_entry)))) {
                                                                                                                                                                                                                                                                                                                                                    if (((IDToInt(((((*in_msg_ptr)).m_Requestor).getNum()))) >= numproc_c)) {
                                                                                                                                                                                                                                                                                                                                                            if ((((*cache_entry).m_finalDestinationSet_NC).isEmpty())) {
                                                                                                                                                                                                                                                                                                                                                                (((*cache_entry).m_finalDestinationSet_NC).add(((*in_msg_ptr)).m_Requestor));
                                                                                                                                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                                                                                                                                        } else {
                                                                                                                                                                                                                                                                                                                                                                if (((IDToInt(((((*in_msg_ptr)).m_Requestor).getNum()))) < numproc_c)) {
                                                                                                                                                                                                                                                                                                                                                                        if ((((*cache_entry).m_finalDestinationSet_C).isEmpty())) {
                                                                                                                                                                                                                                                                                                                                                                            (((*cache_entry).m_finalDestinationSet_C).add(((*in_msg_ptr)).m_Requestor));
                                                                                                                                                                                                                                                                                                                                                                        }
                                                                                                                                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                                                                                                                                }
                                                                                                                                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                                                                                                                                                if (((IDToInt(((((*in_msg_ptr)).m_Requestor).getNum()))) < numproc_c)) {
                                                                                                                                                                                                                                                                                                                                                                    {

                                                                                                                                                                                                                                                                                                                                                                        TransitionResult result = doTransition(L1Cache_Event_Other_GETM_C, cache_entry, tbe, ((*in_msg_ptr)).m_Addr); 

                                                                                                                                                                                                                                                                                                                                                                        if (result == TransitionResult_Valid) {
                                                                                                                                                                                                                                                                                                                                                                            counter++;
                                                                                                                                                                                                                                                                                                                                                                            continue; // Check the first port again
                                                                                                                                                                                                                                                                                                                                                                        }

                                                                                                                                                                                                                                                                                                                                                                        if (result == TransitionResult_ResourceStall) {
                                                                                                                                                                                                                                                                                                                                                                            scheduleEvent(Cycles(1));

                                                                                                                                                                                                                                                                                                                                                                            // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                                                                                                                                                                                                                                                                                                                                                        }
                                                                                                                                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                                                                                                                                    ;
                                                                                                                                                                                                                                                                                                                                                                } else {
                                                                                                                                                                                                                                                                                                                                                                    {

                                                                                                                                                                                                                                                                                                                                                                        TransitionResult result = doTransition(L1Cache_Event_Other_GETM_NC, cache_entry, tbe, ((*in_msg_ptr)).m_Addr); 

                                                                                                                                                                                                                                                                                                                                                                        if (result == TransitionResult_Valid) {
                                                                                                                                                                                                                                                                                                                                                                            counter++;
                                                                                                                                                                                                                                                                                                                                                                            continue; // Check the first port again
                                                                                                                                                                                                                                                                                                                                                                        }

                                                                                                                                                                                                                                                                                                                                                                        if (result == TransitionResult_ResourceStall) {
                                                                                                                                                                                                                                                                                                                                                                            scheduleEvent(Cycles(1));

                                                                                                                                                                                                                                                                                                                                                                            // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                                                                                                                                                                                                                                                                                                                                                        }
                                                                                                                                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                                                                                                                                    ;
                                                                                                                                                                                                                                                                                                                                                                }
                                                                                                                                                                                                                                                                                                                                                            } else {
                                                                                                                                                                                                                                                                                                                                                                    if (((is_dataSendState(tbe, ((*in_msg_ptr)).m_Addr, cache_entry)) || (is_dataInvState(tbe, ((*in_msg_ptr)).m_Addr, cache_entry)))) {
                                                                                                                                                                                                                                                                                                                                                                            if (((IDToInt(((((*in_msg_ptr)).m_Requestor).getNum()))) >= numproc_c)) {
                                                                                                                                                                                                                                                                                                                                                                                    if ((((*cache_entry).m_finalDestinationSet_NC).isEmpty())) {
                                                                                                                                                                                                                                                                                                                                                                                        (((*cache_entry).m_finalDestinationSet_NC).add(((*in_msg_ptr)).m_Requestor));
                                                                                                                                                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                                                                                                                                                } else {
                                                                                                                                                                                                                                                                                                                                                                                        if (((IDToInt(((((*in_msg_ptr)).m_Requestor).getNum()))) < numproc_c)) {
                                                                                                                                                                                                                                                                                                                                                                                                if ((((*cache_entry).m_finalDestinationSet_C).isEmpty())) {
                                                                                                                                                                                                                                                                                                                                                                                                    (((*cache_entry).m_finalDestinationSet_C).add(((*in_msg_ptr)).m_Requestor));
                                                                                                                                                                                                                                                                                                                                                                                                }
                                                                                                                                                                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                                                                                                                                                                        }
                                                                                                                                                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                                                                                                                                                        if ((((IDToInt(((((*in_msg_ptr)).m_Requestor).getNum()))) < numproc_c) && ((IDToInt(((m_machineID).getNum()))) >= numproc_c))) {
                                                                                                                                                                                                                                                                                                                                                                                            {

                                                                                                                                                                                                                                                                                                                                                                                                TransitionResult result = doTransition(L1Cache_Event_Other_GETM_C, cache_entry, tbe, ((*in_msg_ptr)).m_Addr); 

                                                                                                                                                                                                                                                                                                                                                                                                if (result == TransitionResult_Valid) {
                                                                                                                                                                                                                                                                                                                                                                                                    counter++;
                                                                                                                                                                                                                                                                                                                                                                                                    continue; // Check the first port again
                                                                                                                                                                                                                                                                                                                                                                                                }

                                                                                                                                                                                                                                                                                                                                                                                                if (result == TransitionResult_ResourceStall) {
                                                                                                                                                                                                                                                                                                                                                                                                    scheduleEvent(Cycles(1));

                                                                                                                                                                                                                                                                                                                                                                                                    // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                                                                                                                                                                                                                                                                                                                                                                                }
                                                                                                                                                                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                                                                                                                                                                            ;
                                                                                                                                                                                                                                                                                                                                                                                        } else {
                                                                                                                                                                                                                                                                                                                                                                                            {

                                                                                                                                                                                                                                                                                                                                                                                                TransitionResult result = doTransition(L1Cache_Event_Other_GETM_NC, cache_entry, tbe, ((*in_msg_ptr)).m_Addr); 

                                                                                                                                                                                                                                                                                                                                                                                                if (result == TransitionResult_Valid) {
                                                                                                                                                                                                                                                                                                                                                                                                    counter++;
                                                                                                                                                                                                                                                                                                                                                                                                    continue; // Check the first port again
                                                                                                                                                                                                                                                                                                                                                                                                }

                                                                                                                                                                                                                                                                                                                                                                                                if (result == TransitionResult_ResourceStall) {
                                                                                                                                                                                                                                                                                                                                                                                                    scheduleEvent(Cycles(1));

                                                                                                                                                                                                                                                                                                                                                                                                    // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                                                                                                                                                                                                                                                                                                                                                                                }
                                                                                                                                                                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                                                                                                                                                                            ;
                                                                                                                                                                                                                                                                                                                                                                                        }
                                                                                                                                                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                                                                                                                                                }
                                                                                                                                                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                                                                                                                                                        }
                                                                                                                                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                                                                                                                                } else {
                                                                                                                                                                                                                                                                                                                                                                        if ((((*in_msg_ptr)).m_Type == CoherenceRequestType_PUTM)) {
                                                                                                                                                                                                                                                                                                                                                                                if ((((*in_msg_ptr)).m_Requestor == m_machineID)) {
                                                                                                                                                                                                                                                                                                                                                                                    {

                                                                                                                                                                                                                                                                                                                                                                                        TransitionResult result = doTransition(L1Cache_Event_OWN_PUTM, cache_entry, tbe, ((*in_msg_ptr)).m_Addr); 

                                                                                                                                                                                                                                                                                                                                                                                        if (result == TransitionResult_Valid) {
                                                                                                                                                                                                                                                                                                                                                                                            counter++;
                                                                                                                                                                                                                                                                                                                                                                                            continue; // Check the first port again
                                                                                                                                                                                                                                                                                                                                                                                        }

                                                                                                                                                                                                                                                                                                                                                                                        if (result == TransitionResult_ResourceStall) {
                                                                                                                                                                                                                                                                                                                                                                                            scheduleEvent(Cycles(1));

                                                                                                                                                                                                                                                                                                                                                                                            // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                                                                                                                                                                                                                                                                                                                                                                        }
                                                                                                                                                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                                                                                                                                                    ;
                                                                                                                                                                                                                                                                                                                                                                                } else {
                                                                                                                                                                                                                                                                                                                                                                                    {

                                                                                                                                                                                                                                                                                                                                                                                        TransitionResult result = doTransition(L1Cache_Event_Other_PUTM, cache_entry, tbe, ((*in_msg_ptr)).m_Addr); 

                                                                                                                                                                                                                                                                                                                                                                                        if (result == TransitionResult_Valid) {
                                                                                                                                                                                                                                                                                                                                                                                            counter++;
                                                                                                                                                                                                                                                                                                                                                                                            continue; // Check the first port again
                                                                                                                                                                                                                                                                                                                                                                                        }

                                                                                                                                                                                                                                                                                                                                                                                        if (result == TransitionResult_ResourceStall) {
                                                                                                                                                                                                                                                                                                                                                                                            scheduleEvent(Cycles(1));

                                                                                                                                                                                                                                                                                                                                                                                            // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                                                                                                                                                                                                                                                                                                                                                                        }
                                                                                                                                                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                                                                                                                                                    ;
                                                                                                                                                                                                                                                                                                                                                                                }
                                                                                                                                                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                                                                                                                                                        }
                                                                                                                                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                                                                                                                                }
                                                                                                                                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                                                                                                                                        }
            // L1CacheInPort requestNetworkWB_in
            m_cur_in_port = 0;
                        if ((((*m_requestToCacheWB_ptr)).isReady())) {
                            {
                                // Declare message
                                const RequestMsg* in_msg_ptr M5_VAR_USED;
                                in_msg_ptr = dynamic_cast<const RequestMsg *>(((*m_requestToCacheWB_ptr)).peek());
                                assert(in_msg_ptr != NULL); // Check the cast result
                                if (m_is_blocking &&
                                    (m_block_map.count(in_msg_ptr->m_Addr) == 1) &&
                                    (m_block_map[in_msg_ptr->m_Addr] == &(*m_requestToCacheWB_ptr))) {
                                        (*m_requestToCacheWB_ptr).delayHead();
                                        DPRINTF(RubySlicc, "Delaying in_msg_ptr %s\n", *in_msg_ptr);
                                        continue;
                                }
                                            
                            L1Cache_Entry* cache_entry
                             = (getL1CacheEntry(((*in_msg_ptr)).m_Addr));
                            L1Cache_TBE* tbe
                             = (((*m_TBEs_ptr)).lookup(((*in_msg_ptr)).m_Addr));
                            #ifndef NDEBUG
                            if (!(((((*in_msg_ptr)).m_Destination).isElement(m_machineID)))) {
                                panic("Runtime Error at MSI_Snooping_Time_based_base-cache.sm:816: %s.\n", "assert failure");

                            }
                            #endif
                            ;
                                if ((((*in_msg_ptr)).m_Type == CoherenceRequestType_INV)) {
                                    {

                                        TransitionResult result = doTransition(L1Cache_Event_INV, cache_entry, tbe, ((*in_msg_ptr)).m_Addr); 

                                        if (result == TransitionResult_Valid) {
                                            counter++;
                                            continue; // Check the first port again
                                        }

                                        if (result == TransitionResult_ResourceStall) {
                                            scheduleEvent(Cycles(1));

                                            // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                        }
                                    }
                                    ;
                                } else {
                                        if ((((*in_msg_ptr)).m_Type == CoherenceRequestType_PUTM)) {
                                                if ((((*in_msg_ptr)).m_Requestor == m_machineID)) {
                                                    {

                                                        TransitionResult result = doTransition(L1Cache_Event_OWN_PUTM, cache_entry, tbe, ((*in_msg_ptr)).m_Addr); 

                                                        if (result == TransitionResult_Valid) {
                                                            counter++;
                                                            continue; // Check the first port again
                                                        }

                                                        if (result == TransitionResult_ResourceStall) {
                                                            scheduleEvent(Cycles(1));

                                                            // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                                        }
                                                    }
                                                    ;
                                                } else {
                                                    {

                                                        TransitionResult result = doTransition(L1Cache_Event_Other_PUTM, cache_entry, tbe, ((*in_msg_ptr)).m_Addr); 

                                                        if (result == TransitionResult_Valid) {
                                                            counter++;
                                                            continue; // Check the first port again
                                                        }

                                                        if (result == TransitionResult_ResourceStall) {
                                                            scheduleEvent(Cycles(1));

                                                            // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                                        }
                                                    }
                                                    ;
                                                }
                                            } else {
                                                    if ((((*in_msg_ptr)).m_Type == CoherenceRequestType_SENDDATA)) {
                                                            if ((((*in_msg_ptr)).m_Requestor == m_machineID)) {
                                                                {

                                                                    TransitionResult result = doTransition(L1Cache_Event_Own_SENDDATA, cache_entry, tbe, ((*in_msg_ptr)).m_Addr); 

                                                                    if (result == TransitionResult_Valid) {
                                                                        counter++;
                                                                        continue; // Check the first port again
                                                                    }

                                                                    if (result == TransitionResult_ResourceStall) {
                                                                        scheduleEvent(Cycles(1));

                                                                        // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                                                    }
                                                                }
                                                                ;
                                                            } else {
                                                                {

                                                                    TransitionResult result = doTransition(L1Cache_Event_Other_SENDDATA, cache_entry, tbe, ((*in_msg_ptr)).m_Addr); 

                                                                    if (result == TransitionResult_Valid) {
                                                                        counter++;
                                                                        continue; // Check the first port again
                                                                    }

                                                                    if (result == TransitionResult_ResourceStall) {
                                                                        scheduleEvent(Cycles(1));

                                                                        // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                                                    }
                                                                }
                                                                ;
                                                            }
                                                        } else {
                                                                if ((((*in_msg_ptr)).m_Type == CoherenceRequestType_SELFINV)) {
                                                                        if ((((*in_msg_ptr)).m_Requestor == m_machineID)) {
                                                                                if (((((*in_msg_ptr)).m_DataDestination).isElement(((*in_msg_ptr)).m_Requestor))) {
                                                                                    {

                                                                                        TransitionResult result = doTransition(L1Cache_Event_Own_SELFINV_SPL, cache_entry, tbe, ((*in_msg_ptr)).m_Addr); 

                                                                                        if (result == TransitionResult_Valid) {
                                                                                            counter++;
                                                                                            continue; // Check the first port again
                                                                                        }

                                                                                        if (result == TransitionResult_ResourceStall) {
                                                                                            scheduleEvent(Cycles(1));

                                                                                            // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                                                                        }
                                                                                    }
                                                                                    ;
                                                                                } else {
                                                                                    {

                                                                                        TransitionResult result = doTransition(L1Cache_Event_Own_SELFINV, cache_entry, tbe, ((*in_msg_ptr)).m_Addr); 

                                                                                        if (result == TransitionResult_Valid) {
                                                                                            counter++;
                                                                                            continue; // Check the first port again
                                                                                        }

                                                                                        if (result == TransitionResult_ResourceStall) {
                                                                                            scheduleEvent(Cycles(1));

                                                                                            // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                                                                        }
                                                                                    }
                                                                                    ;
                                                                                }
                                                                            } else {
                                                                                {

                                                                                    TransitionResult result = doTransition(L1Cache_Event_Other_SELFINV, cache_entry, tbe, ((*in_msg_ptr)).m_Addr); 

                                                                                    if (result == TransitionResult_Valid) {
                                                                                        counter++;
                                                                                        continue; // Check the first port again
                                                                                    }

                                                                                    if (result == TransitionResult_ResourceStall) {
                                                                                        scheduleEvent(Cycles(1));

                                                                                        // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                                                                    }
                                                                                }
                                                                                ;
                                                                            }
                                                                        } else {
                                                                                if ((((*in_msg_ptr)).m_Type == CoherenceRequestType_ALLINV)) {
                                                                                    {

                                                                                        TransitionResult result = doTransition(L1Cache_Event_ALLINV, cache_entry, tbe, ((*in_msg_ptr)).m_Addr); 

                                                                                        if (result == TransitionResult_Valid) {
                                                                                            counter++;
                                                                                            continue; // Check the first port again
                                                                                        }

                                                                                        if (result == TransitionResult_ResourceStall) {
                                                                                            scheduleEvent(Cycles(1));

                                                                                            // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                                                                        }
                                                                                    }
                                                                                    ;
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                }
                                                            }
            // L1CacheInPort mandatoryQueue_in
            m_cur_in_port = 0;
                        if ((((*m_mandatoryQueue_ptr)).isReady())) {
                                if ((m_is_blocked == (0))) {
                                    {
                                        // Declare message
                                        const RubyRequest* in_msg_ptr M5_VAR_USED;
                                        in_msg_ptr = dynamic_cast<const RubyRequest *>(((*m_mandatoryQueue_ptr)).peek());
                                        assert(in_msg_ptr != NULL); // Check the cast result
                                        if (m_is_blocking &&
                                            (m_block_map.count(in_msg_ptr->m_LineAddress) == 1) &&
                                            (m_block_map[in_msg_ptr->m_LineAddress] == &(*m_mandatoryQueue_ptr)) &&       
                                            !(in_msg_ptr->m_PType == RubyRequestType::RubyRequestType_Locked_RMW_Write)) {
                                                (*m_mandatoryQueue_ptr).delayHead();
                                                DPRINTF(RubySlicc, "Ruby request Delaying in_msg_ptr %s \n", *in_msg_ptr);
                                                continue;
                                        }
                                                    
                                    L1Cache_Entry* cache_entry
                                     = (getL1CacheEntry(((*in_msg_ptr)).m_LineAddress));
                                        if ((((*in_msg_ptr)).m_Type == RubyRequestType_IFETCH)) {
                                                if (((cache_entry == NULL) && ((((*m_Icache_ptr)).cacheAvail(((*in_msg_ptr)).m_LineAddress)) == (false)))) {
                                                    {

                                                        TransitionResult result = doTransition(L1Cache_Event_Replacement, (getL1CacheEntry((((*m_Icache_ptr)).cacheProbe(((*in_msg_ptr)).m_LineAddress)))), (((*m_TBEs_ptr)).lookup((((*m_Icache_ptr)).cacheProbe(((*in_msg_ptr)).m_LineAddress)))), (((*m_Icache_ptr)).cacheProbe(((*in_msg_ptr)).m_LineAddress))); 

                                                        if (result == TransitionResult_Valid) {
                                                            counter++;
                                                            continue; // Check the first port again
                                                        }

                                                        if (result == TransitionResult_ResourceStall) {
                                                            scheduleEvent(Cycles(1));

                                                            // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                                        }
                                                    }
                                                    ;
                                                } else {
                                                    {

                                                        TransitionResult result = doTransition((mandatory_request_type_to_event(((*in_msg_ptr)).m_Type)), cache_entry, (((*m_TBEs_ptr)).lookup(((*in_msg_ptr)).m_LineAddress)), ((*in_msg_ptr)).m_LineAddress); 

                                                        if (result == TransitionResult_Valid) {
                                                            counter++;
                                                            continue; // Check the first port again
                                                        }

                                                        if (result == TransitionResult_ResourceStall) {
                                                            scheduleEvent(Cycles(1));

                                                            // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                                        }
                                                    }
                                                    ;
                                                }
                                            } else {
                                                    if (((cache_entry == NULL) && ((((*m_Dcache_ptr)).cacheAvail(((*in_msg_ptr)).m_LineAddress)) == (false)))) {
                                                        {

                                                            TransitionResult result = doTransition(L1Cache_Event_Replacement, (getL1CacheEntry((((*m_Dcache_ptr)).cacheProbe(((*in_msg_ptr)).m_LineAddress)))), (((*m_TBEs_ptr)).lookup((((*m_Dcache_ptr)).cacheProbe(((*in_msg_ptr)).m_LineAddress)))), (((*m_Dcache_ptr)).cacheProbe(((*in_msg_ptr)).m_LineAddress))); 

                                                            if (result == TransitionResult_Valid) {
                                                                counter++;
                                                                continue; // Check the first port again
                                                            }

                                                            if (result == TransitionResult_ResourceStall) {
                                                                scheduleEvent(Cycles(1));

                                                                // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                                            }
                                                        }
                                                        ;
                                                    } else {
                                                        {

                                                            TransitionResult result = doTransition((mandatory_request_type_to_event(((*in_msg_ptr)).m_Type)), cache_entry, (((*m_TBEs_ptr)).lookup(((*in_msg_ptr)).m_LineAddress)), ((*in_msg_ptr)).m_LineAddress); 

                                                            if (result == TransitionResult_Valid) {
                                                                counter++;
                                                                continue; // Check the first port again
                                                            }

                                                            if (result == TransitionResult_ResourceStall) {
                                                                scheduleEvent(Cycles(1));

                                                                // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                                            }
                                                        }
                                                        ;
                                                    }
                                                }
                                                }
                                            } else {
                                                {
                                                    // Declare message
                                                    const RubyRequest* in_msg_ptr M5_VAR_USED;
                                                    in_msg_ptr = dynamic_cast<const RubyRequest *>(((*m_mandatoryQueue_ptr)).peek());
                                                    assert(in_msg_ptr != NULL); // Check the cast result
                                                    if (m_is_blocking &&
                                                        (m_block_map.count(in_msg_ptr->m_LineAddress) == 1) &&
                                                        (m_block_map[in_msg_ptr->m_LineAddress] == &(*m_mandatoryQueue_ptr)) &&       
                                                        !(in_msg_ptr->m_PType == RubyRequestType::RubyRequestType_Locked_RMW_Write)) {
                                                            (*m_mandatoryQueue_ptr).delayHead();
                                                            DPRINTF(RubySlicc, "Ruby request Delaying in_msg_ptr %s \n", *in_msg_ptr);
                                                            continue;
                                                    }
                                                                
                                                L1Cache_Entry* cache_entry
                                                 = (getL1CacheEntry(((*in_msg_ptr)).m_LineAddress));
                                                    if ((((*in_msg_ptr)).m_Type == RubyRequestType_Locked_RMW_Write)) {
                                                        {

                                                            TransitionResult result = doTransition((mandatory_request_type_to_event(((*in_msg_ptr)).m_Type)), cache_entry, (((*m_TBEs_ptr)).lookup(((*in_msg_ptr)).m_LineAddress)), ((*in_msg_ptr)).m_LineAddress); 

                                                            if (result == TransitionResult_Valid) {
                                                                counter++;
                                                                continue; // Check the first port again
                                                            }

                                                            if (result == TransitionResult_ResourceStall) {
                                                                scheduleEvent(Cycles(1));

                                                                // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                                            }
                                                        }
                                                        ;
                                                    }
                                                    }
                                                }
                                            }
            // L1CacheInPort TimerTable_CC_in
            m_cur_in_port = 2;
                        if ((((*m_TimerTable_CC_ptr)).isReady())) {
                            L1Cache_Entry* cacheentry
                             = (getL1CacheEntry((((*m_TimerTable_CC_ptr)).readyAddress())));
                                if ((((((*cacheentry).m_Timeout == m_timeout_latency_CC) || ((*cacheentry).m_CacheState == L1Cache_State_S)) || ((*cacheentry).m_CacheState == L1Cache_State_M)) || ((*cacheentry).m_CacheState == L1Cache_State_S_TM))) {
                                    {

                                        TransitionResult result = doTransition(L1Cache_Event_Timeout_CC, (getL1CacheEntry((((*m_TimerTable_CC_ptr)).readyAddress()))), (((*m_TBEs_ptr)).lookup((((*m_TimerTable_CC_ptr)).readyAddress()))), (((*m_TimerTable_CC_ptr)).readyAddress())); 

                                        if (result == TransitionResult_Valid) {
                                            counter++;
                                            continue; // Check the first port again
                                        }

                                        if (result == TransitionResult_ResourceStall) {
                                            scheduleEvent(Cycles(1));

                                            // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                        }
                                    }
                                    ;
                                } else {
                                    {

                                        TransitionResult result = doTransition(L1Cache_Event_Timeout_noinv, (getL1CacheEntry((((*m_TimerTable_CC_ptr)).readyAddress()))), (((*m_TBEs_ptr)).lookup((((*m_TimerTable_CC_ptr)).readyAddress()))), (((*m_TimerTable_CC_ptr)).readyAddress())); 

                                        if (result == TransitionResult_Valid) {
                                            counter++;
                                            continue; // Check the first port again
                                        }

                                        if (result == TransitionResult_ResourceStall) {
                                            scheduleEvent(Cycles(1));

                                            // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                        }
                                    }
                                    ;
                                }
                            }
            // L1CacheInPort TimerTable_CN_in
            m_cur_in_port = 2;
                        if ((((*m_TimerTable_CN_ptr)).isReady())) {
                            L1Cache_Entry* cacheentry
                             = (getL1CacheEntry((((*m_TimerTable_CN_ptr)).readyAddress())));
                                if ((((((*cacheentry).m_Timeout == m_timeout_latency_CN) || ((*cacheentry).m_CacheState == L1Cache_State_S)) || ((*cacheentry).m_CacheState == L1Cache_State_M)) || ((*cacheentry).m_CacheState == L1Cache_State_S_TM))) {
                                    {

                                        TransitionResult result = doTransition(L1Cache_Event_Timeout_CN, (getL1CacheEntry((((*m_TimerTable_CN_ptr)).readyAddress()))), (((*m_TBEs_ptr)).lookup((((*m_TimerTable_CN_ptr)).readyAddress()))), (((*m_TimerTable_CN_ptr)).readyAddress())); 

                                        if (result == TransitionResult_Valid) {
                                            counter++;
                                            continue; // Check the first port again
                                        }

                                        if (result == TransitionResult_ResourceStall) {
                                            scheduleEvent(Cycles(1));

                                            // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                        }
                                    }
                                    ;
                                } else {
                                    {

                                        TransitionResult result = doTransition(L1Cache_Event_Timeout_noinv_noncric, (getL1CacheEntry((((*m_TimerTable_CN_ptr)).readyAddress()))), (((*m_TBEs_ptr)).lookup((((*m_TimerTable_CN_ptr)).readyAddress()))), (((*m_TimerTable_CN_ptr)).readyAddress())); 

                                        if (result == TransitionResult_Valid) {
                                            counter++;
                                            continue; // Check the first port again
                                        }

                                        if (result == TransitionResult_ResourceStall) {
                                            scheduleEvent(Cycles(1));

                                            // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                        }
                                    }
                                    ;
                                }
                            }
            // L1CacheInPort TimerTable_NC_in
            m_cur_in_port = 2;
                        if ((((*m_TimerTable_NC_ptr)).isReady())) {
                            L1Cache_Entry* cacheentry
                             = (getL1CacheEntry((((*m_TimerTable_NC_ptr)).readyAddress())));
                                if ((((((*(getL1CacheEntry((((*m_TimerTable_NC_ptr)).readyAddress())))).m_Timeout == m_timeout_latency_NC) || ((*cacheentry).m_CacheState == L1Cache_State_S)) || ((*cacheentry).m_CacheState == L1Cache_State_M)) || ((*cacheentry).m_CacheState == L1Cache_State_S_TM))) {
                                    {

                                        TransitionResult result = doTransition(L1Cache_Event_Timeout_NC, (getL1CacheEntry((((*m_TimerTable_NC_ptr)).readyAddress()))), (((*m_TBEs_ptr)).lookup((((*m_TimerTable_NC_ptr)).readyAddress()))), (((*m_TimerTable_NC_ptr)).readyAddress())); 

                                        if (result == TransitionResult_Valid) {
                                            counter++;
                                            continue; // Check the first port again
                                        }

                                        if (result == TransitionResult_ResourceStall) {
                                            scheduleEvent(Cycles(1));

                                            // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                        }
                                    }
                                    ;
                                } else {
                                    {

                                        TransitionResult result = doTransition(L1Cache_Event_Timeout_noinv, (getL1CacheEntry((((*m_TimerTable_NC_ptr)).readyAddress()))), (((*m_TBEs_ptr)).lookup((((*m_TimerTable_NC_ptr)).readyAddress()))), (((*m_TimerTable_NC_ptr)).readyAddress())); 

                                        if (result == TransitionResult_Valid) {
                                            counter++;
                                            continue; // Check the first port again
                                        }

                                        if (result == TransitionResult_ResourceStall) {
                                            scheduleEvent(Cycles(1));

                                            // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                        }
                                    }
                                    ;
                                }
                            }
            // L1CacheInPort TimerTable_NN_in
            m_cur_in_port = 2;
                        if ((((*m_TimerTable_NN_ptr)).isReady())) {
                            L1Cache_Entry* cacheentry
                             = (getL1CacheEntry((((*m_TimerTable_NN_ptr)).readyAddress())));
                                if ((((((*(getL1CacheEntry((((*m_TimerTable_NN_ptr)).readyAddress())))).m_Timeout == m_timeout_latency_NN) || ((*cacheentry).m_CacheState == L1Cache_State_S)) || ((*cacheentry).m_CacheState == L1Cache_State_M)) || ((*cacheentry).m_CacheState == L1Cache_State_S_TM))) {
                                    {

                                        TransitionResult result = doTransition(L1Cache_Event_Timeout_NN, (getL1CacheEntry((((*m_TimerTable_NN_ptr)).readyAddress()))), (((*m_TBEs_ptr)).lookup((((*m_TimerTable_NN_ptr)).readyAddress()))), (((*m_TimerTable_NN_ptr)).readyAddress())); 

                                        if (result == TransitionResult_Valid) {
                                            counter++;
                                            continue; // Check the first port again
                                        }

                                        if (result == TransitionResult_ResourceStall) {
                                            scheduleEvent(Cycles(1));

                                            // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                        }
                                    }
                                    ;
                                } else {
                                    {

                                        TransitionResult result = doTransition(L1Cache_Event_Timeout_noinv_noncric, (getL1CacheEntry((((*m_TimerTable_NN_ptr)).readyAddress()))), (((*m_TBEs_ptr)).lookup((((*m_TimerTable_NN_ptr)).readyAddress()))), (((*m_TimerTable_NN_ptr)).readyAddress())); 

                                        if (result == TransitionResult_Valid) {
                                            counter++;
                                            continue; // Check the first port again
                                        }

                                        if (result == TransitionResult_ResourceStall) {
                                            scheduleEvent(Cycles(1));

                                            // Cannot do anything with this transition, go check next doable transition (mostly likely of next port)
                                        }
                                    }
                                    ;
                                }
                            }
        break;  // If we got this far, we have nothing left todo
    }
}
