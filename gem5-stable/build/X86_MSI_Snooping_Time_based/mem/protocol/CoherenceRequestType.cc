/** \file CoherenceRequestType.hh
 *
 * Auto generated C++ code started by /gem5/gem5-stable/src/mem/slicc/symbols/Type.py:550
 */

#include <cassert>
#include <iostream>
#include <string>

#include "base/misc.hh"
#include "mem/protocol/CoherenceRequestType.hh"

using namespace std;

// Code for output operator
ostream&
operator<<(ostream& out, const CoherenceRequestType& obj)
{
    out << CoherenceRequestType_to_string(obj);
    out << flush;
    return out;
}

// Code to convert state to a string
string
CoherenceRequestType_to_string(const CoherenceRequestType& obj)
{
    switch(obj) {
      case CoherenceRequestType_GETM:
        return "GETM";
      case CoherenceRequestType_GETMA:
        return "GETMA";
      case CoherenceRequestType_GETX:
        return "GETX";
      case CoherenceRequestType_GETS:
        return "GETS";
      case CoherenceRequestType_GETI:
        return "GETI";
      case CoherenceRequestType_INV:
        return "INV";
      case CoherenceRequestType_UPG:
        return "UPG";
      case CoherenceRequestType_PUTM:
        return "PUTM";
      case CoherenceRequestType_GETMATOMICEN:
        return "GETMATOMICEN";
      case CoherenceRequestType_GETMATOMICST:
        return "GETMATOMICST";
      case CoherenceRequestType_WB_ACK:
        return "WB_ACK";
      case CoherenceRequestType_DMA_READ:
        return "DMA_READ";
      case CoherenceRequestType_DMA_WRITE:
        return "DMA_WRITE";
      case CoherenceRequestType_SELFINV:
        return "SELFINV";
      case CoherenceRequestType_SENDDATA:
        return "SENDDATA";
      case CoherenceRequestType_ALLINV:
        return "ALLINV";
      default:
        panic("Invalid range for type CoherenceRequestType");
    }
}

// Code to convert from a string to the enumeration
CoherenceRequestType
string_to_CoherenceRequestType(const string& str)
{
    if (str == "GETM") {
        return CoherenceRequestType_GETM;
    } else if (str == "GETMA") {
        return CoherenceRequestType_GETMA;
    } else if (str == "GETX") {
        return CoherenceRequestType_GETX;
    } else if (str == "GETS") {
        return CoherenceRequestType_GETS;
    } else if (str == "GETI") {
        return CoherenceRequestType_GETI;
    } else if (str == "INV") {
        return CoherenceRequestType_INV;
    } else if (str == "UPG") {
        return CoherenceRequestType_UPG;
    } else if (str == "PUTM") {
        return CoherenceRequestType_PUTM;
    } else if (str == "GETMATOMICEN") {
        return CoherenceRequestType_GETMATOMICEN;
    } else if (str == "GETMATOMICST") {
        return CoherenceRequestType_GETMATOMICST;
    } else if (str == "WB_ACK") {
        return CoherenceRequestType_WB_ACK;
    } else if (str == "DMA_READ") {
        return CoherenceRequestType_DMA_READ;
    } else if (str == "DMA_WRITE") {
        return CoherenceRequestType_DMA_WRITE;
    } else if (str == "SELFINV") {
        return CoherenceRequestType_SELFINV;
    } else if (str == "SENDDATA") {
        return CoherenceRequestType_SENDDATA;
    } else if (str == "ALLINV") {
        return CoherenceRequestType_ALLINV;
    } else {
        panic("Invalid string conversion for %s, type CoherenceRequestType", str);
    }
}

// Code to increment an enumeration type
CoherenceRequestType&
operator++(CoherenceRequestType& e)
{
    assert(e < CoherenceRequestType_NUM);
    return e = CoherenceRequestType(e+1);
}
