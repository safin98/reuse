FROM ubuntu:14.04
MAINTAINER amkaushi@uwaterloo.ca

# Download and Install utilities
# ==============================
RUN rm /bin/sh && ln -s /bin/bash /bin/sh
RUN apt-get update \
  && apt-get install -y \
    vim \
		git 


RUN apt-get install -y swig gcc m4 python python-dev libgoogle-perftools-dev mercurial scons g++ build-essential zlib1g-dev wget

## cleanup
RUN apt-get clean && \
    cd /var/lib/apt/lists && rm -fr *Release* *Sources* *Packages* && \
    truncate -s 0 /var/log/*log
#Set env just in case
#ENV HOME /root
ENV PATH /usr/local/rvm/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

# Download Pendulum
RUN bash -l -c "git clone https://gitlab.com/safin98/reuse.git"

# Build Pendulum
RUN bash -l -c "cd /reuse/gem5-stable; scons --clean; scons ./build/X86_MSI_Snooping_Time_based/gem5.opt -j8"
